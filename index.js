const {app, BrowserWindow, dialog} = require("electron")
const path = require('path')


app.on('ready', () =>{
  let mainWindow = new BrowserWindow({height:900, width:1250,  webPreferences: {
    nodeIntegration: true
  }})
  mainWindow.loadURL(`file://${__dirname}/login.html`)
  mainWindow.once("ready-to-show", () => {mainWindow.show()})

});

app.on("window-all-closed", () => {app.quit()})
