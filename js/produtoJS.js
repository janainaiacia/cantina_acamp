$(document).ready(function () {
    $("#iptPreco").maskMoney();
});

function deleteRow(btn) {

    if (btn.value !== '') { //deletar do banco
        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let sql = `DEPETE FROM PRODUTOS_PRECO WHERE PRO_ID = ${$("#iptIdProd").val()} AND  PRO_QTDE = ${btn.value}`;
        let row = sqlite.run(sql);

        if (row === 0 || row === 1) {
            alert_custom("S", "Preço excluido.");
        }
        else {
            alert_custom("E", row.error);
        }
        sqlite.close();
    }

    let tr_delete = $(btn).closest('tr');
    tr_delete.remove();
}

function addPrice() {
    if(validaAddPrice()){
        let table = $("#tbPreco")[0];
        let row = table.insertRow(table.rows.length);

        let cell1 = row.insertCell(0);
        let cell2 = row.insertCell(1);
        let cell3 = row.insertCell(2);

        cell1.innerHTML = $("#iptQtde").val();
        cell2.innerHTML = $("#iptPreco").val();
        cell3.innerHTML = `<button type='button' class='btn btn-sm'
                            style='color:red;' value=''
                                onclick='deleteCell(this);'>
                            <i class='fa fa-times'></i>
                        </button>`;

        clearPrice();
    }
}

function validaAddPrice(){
    let table = $("#tbPreco")[0];
    let found = true;

    let msg = "";

    if($("#iptQtde").val() === "")
    {
        msg += "Informe a quantidade." + "<br />";
    }
    else
    {
        if(parseInt($("#iptQtde").val()) === 0)
        {
            msg += "Quantidade deve ser maior que 0" + "<br />";
        }
    }

    if($("#iptPreco").val() === "")
    {
        msg += "Informe o preço." + "<br />";
    }

    for(let i = 0; i < table.rows.length; i++){
        
        if($("#iptQtde").val() === table.rows[i].cells[0].innerHTML.toString()){
            msg += "Quantidade já inserida" + "<br />"
        }
    }

    if(msg !== ""){
        alert_custom("W", msg);
        return false;
    }
    else
        return true;
}

function clearPrice() {
    $("#iptQtde").val("");
    $("#iptPreco").val("");
    $("#iptQtde").focus();
}

function btnNovo_Click(focus) {

    $("#tbPreco").find("tr:gt(0)").remove();
    $("#tbProd").find("tr:gt(0)").remove();
    
    $("#iptIdProd").val("");
    $("#iptNameProd").val("");
    $("#ckbAtivo").prop("checked", true);
    $("#iptQtde").val("");
    $("#iptPreco").val("");

    $("#btnExcluir").hide();

    if (focus != false)
        $("#iptIdProd").focus();
}

function btnSalvar_Click() {
    if (validaCampos()) {
        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        if ($("#ckbAtivo").is(":checked")) {
            ativo = "S"
        } else {
            ativo = "N";
        }

        let sql = "";
        if ($("#btnExcluir").is(":visible")) { //Update
            sql = `UPDATE PRODUTOS SET PRO_DESCRIPTION = '${$("#iptNameProd").val()}', PRO_ACTIVE = '${ativo}' WHERE PRO_ID = ${$("#iptIdProd").val()}`;

            sqlite.run(sql, [], function (err) {
                if (err.error != undefined) {
                    alert_custom("E", err.error);
                }
                else {
                    let table = $("#tbPreco")[0];
                    let id = err;
                    let erro = "";
                    let param = [];
                    for (let i = 1; i < table.rows.length; i++) {
                        let row = sqlite.run(`SELECT * FROM PRODUTOS_PRECO WHERE PRO_ID = ${$("#iptIdProd").val()} AND PRO_QTDE = ${table.rows[i].cells[0].innerHTML}`);
                        if (row.length > 0) {
                            sql = `UPDATE PRODUTOS_PRECO SET PRO_PRECO = ? WHERE PRO_ID = ? AND PRO_QTDE = ? `;
                            param = [table.rows[i].cells[1].innerHTML.replaceAll(".", "").replaceAll(",", "."), table.rows[i].cells[0].innerHTML, $("#iptIdProd").val()];
                        }
                        else {
                            sql = `INSERT INTO PRODUTOS_PRECO (PRO_ID, PRO_QTDE, PRO_PRECO) VALUES (?,?,?)`;
                            param = [$("#iptIdProd").val(), table.rows[i].cells[0].innerHTML, table.rows[i].cells[1].innerHTML.replaceAll(".", "").replaceAll(",", ".")];
                        }

                        sqlite.run(sql, param, function (err) {
                            if (err.error != undefined) {
                                alert_custom("E", err.error);
                                erro = err.error;
                                i = table.rows.length;
                            }
                        });
                    }

                    if (erro === "") {
                        alert_custom("S", "Produto alterado.")
                        btnNovo_Click();
                    }
                }
            });
        }
        else {
            sql = `INSERT INTO PRODUTOS (PRO_DESCRIPTION, PRO_ACTIVE) VALUES ('${$("#iptNameProd").val()}','${ativo}')`;
            if ($("#iptIdProd").val() !== "") {
                sql = `INSERT INTO PRODUTOS (PRO_ID, PRO_DESCRIPTION, PRO_ACTIVE) VALUES
             (${parseInt($("#iptIdProd").val())}, '${$("#iptNameProd").val()}', '${ativo}')`;
            }

            sqlite.run(sql, [], function (err) {
                if (err.error != undefined) {
                    alert_custom("E", err.error);
                }
                else {
                    let table = $("#tbPreco")[0];
                    let id = err;
                    let erro = "";
                    sql = `INSERT INTO PRODUTOS_PRECO (PRO_ID, PRO_QTDE, PRO_PRECO) VALUES (?,?,?)`;
                    for (let i = 1; i < table.rows.length; i++) {

                        sqlite.run(sql,
                            [id, table.rows[i].cells[0].innerHTML, table.rows[i].cells[1].innerHTML.replaceAll(".", "").replaceAll(",", ".")], function (err) {
                                if (err.error != undefined) {
                                    alert_custom("E", err.error);
                                    erro = err.error;
                                    i = table.rows.length;
                                }
                            });
                    }

                    if (erro === "") {
                        alert_custom("S", "Produto salvo.")
                        btnNovo_Click();
                    }
                    else {
                        sqlite.run(`DELETE FROM PRODUTOS_PRECO WHERE PRO_ID = ${id}`);
                        sqlite.run(`DELETE FROM PRODUTOS WHERE PRO_ID = ${id}`);
                    }
                }
            });
        }
        sqlite.close();
    }
}

function validaCampos() {
    let msg = "";

    if ($("#iptNameProd").val() === "") {
        msg += "informe o nome do produto." + "<br />";
    }

    if ($("#tbPreco")[0].rows.length === 1) {//considera cabeçalho, por isso começa de 1
        msg += "Insira ao menos um preço." + "<br />";
    }

    if (msg !== "") {
        alert_custom("W", msg);
        return false;
    }
    else
        return true;
}

function btnExcluir_Click() {
    if (deleteProductID($("#iptIdProd").val())) {
        btnNovo_Click();
    }
}

function btnConsulta_Click() {
    $("#tbProd").find("tr:gt(0)").remove();
    let row = getProdLikeName($("#iptNameProdC").val());
    let table = $("#tbProd")[0];

    for (let i = 0; i < row.length; i++) {
        let rowTable = table.insertRow(table.rows.length);

        let cell1 = rowTable.insertCell(0);
        let cell2 = rowTable.insertCell(1);
        let cell3 = rowTable.insertCell(2);
        let cell4 = rowTable.insertCell(3);
        let cell5 = rowTable.insertCell(4);
        let cell6 = rowTable.insertCell(5);

        cell1.innerHTML = row[i].PRO_ID;
        cell2.innerHTML = row[i].PRO_DESCRIPTION;


        if (row[i].PRO_ACTIVE === "S") {
            cell3.innerHTML = `<input type="checkbox" name="ckbAtivo" value="${row[i].PRO_ID}" checked onchange="setCkbAtivoInativo(this);"> Ativo?`
        } else {
            cell3.innerHTML = `<input type="checkbox" name="ckbAtivo" value="${row[i].PRO_ID}" onchange="setCkbAtivoInativo(this);"> Ativo?`
        }

        cell4.innerHTML = `<button type='button' class='btn btn-sm'
                onclick='seePrices(${row[i].PRO_ID}, "${row[i].PRO_DESCRIPTION}");'>
                <i class="far fa-list-alt"></i>
        </button>`;

        cell5.innerHTML = `<button type='button' class='btn btn-sm'
                                onclick='editProduct(${row[i].PRO_ID});'>
                                <i class="fas fa-edit"></i>
                        </button>`;

        cell6.innerHTML = `<button type='button' class='btn btn-sm'
            style='color:red;' onclick='deleteProduct(${row[i].PRO_ID}, this);'>
            <i class='fa fa-times'></i>
        </button>`;
    }
}

function getProdutoCod_Blur() {
    if ($("#iptIdProd").val() !== "") {
        let row = getProdCod($("#iptIdProd").val());

        if (row !== undefined && row.length > 0) {
            $("#iptIdProd").val(row[0].PRO_ID);
            $("#iptNameProd").val(row[0].PRO_DESCRIPTION);

            if (row[0].PRO_ACTIVE === 'S')
                $("#ckbAtivo").prop("checked", true);
            else
                $("#ckbAtivo").prop("checked", false);

            row = getProdutoPrice($("#iptIdProd").val());

            if (row.length > 0) {
                $("#tbPreco").find("tr:gt(0)").remove();
                let table = $("#tbPreco")[0];

                for (let i = 0; i < row.length; i++) {
                    let rowTable = table.insertRow(table.rows.length);

                    let cell1 = rowTable.insertCell(0);
                    let cell2 = rowTable.insertCell(1);

                    cell1.innerHTML = row[i].PRO_QTDE;
                    cell2.innerHTML = row[i].PRO_PRECO;
                }
            }
            $("#btnExcluir").show();
        }
        else{
            $("#tbPreco").find("tr:gt(0)").remove();

            $("#iptNameProd").val("");
            $("#ckbAtivo").prop("checked", true);
            $("#iptQtde").val("");
            $("#iptPreco").val("");
        
            $("#btnExcluir").hide();
        }
    }
    else
        btnNovo_Click(false);
}

function getProdutoPrice(id) {
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let row = sqlite.run(`SELECT PRO_QTDE, printf("%.2f", PRO_PRECO) AS PRO_PRECO FROM PRODUTOS_PRECO WHERE PRO_ID = ${id}`);
    sqlite.close();

    return row;
}

function getProdCod(id) {
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let sql = `SELECT * FROM PRODUTOS WHERE PRO_ID = ?`;

    let row = sqlite.run(sql, [id]);
    sqlite.close();
    
    return row;
}

function getProdLikeName(name) {
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let sql = `SELECT * FROM PRODUTOS WHERE upper(PRO_DESCRIPTION) LIKE upper('%${name}%');`;

    let row = sqlite.run(sql);

    
    if (row.error != undefined) {
        alert_custom("E", row.error);
    }
    else{
        return row;
    }

    sqlite.close();
}

function setCkbAtivoInativo(ckb) {
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');
    let ativo = "";
    if ($(ckb).is(":checked")) {
        ativo = "S"
    } else {
        ativo = "N";
    }

    sqlite.run(`UPDATE PRODUTOS SET PRO_ACTIVE = '${ativo}' WHERE PRO_ID = ${ckb.value}`, [], function (err) {
        if (err.error != undefined) {
            alert_custom("E", err.error);
        }
        else {
            alert_custom("S", "Produto alterado.");
        }
    });
    sqlite.close();
}

function seePrices(id, name) {

    let row = getProdutoPrice(id);

    if (row.length > 0) {
        $("#modalPricesLabel").text(name);

        $("#tbModalPrice").find("tr:gt(0)").remove();
        let table = $("#tbModalPrice")[0];

        for (let i = 0; i < row.length; i++) {
            let rowTable = table.insertRow(table.rows.length);

            let cell1 = rowTable.insertCell(0);
            let cell2 = rowTable.insertCell(1);

            cell1.className = "text-center";
            cell2.className = "text-center";

            cell1.innerHTML = row[i].PRO_QTDE;
            cell2.innerHTML = row[i].PRO_PRECO;
        }

        $("#modalPrices").modal('show');
    }
}

function editProduct(id) {
    $("#iptIdProd").val(id);
    getProdutoCod_Blur();
    changeNav('a1', 'form-cadastro');
}

function deleteProduct(id, btn) {
    let deletado = deleteProductID(id);
    if (deletado) {
        let tr_delete = $(btn).closest('tr');
        tr_delete.remove();
        btnNovo_Click();
    }
}

function deleteProductID(id) {
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let deletado = false;

    sqlite.run(`DELETE FROM PRODUTOS_PRECO WHERE PRO_ID = ?`, [id], function (err) {
        if (err.error != undefined) {
            alert_custom("E", err.error);
            deletado = false;
        }
        else {
            sqlite.run(`DELETE FROM PRODUTOS WHERE PRO_ID = ?`, [id], function (err) {
                if (err.error != undefined) {
                    alert_custom("E", err.error);
                    deletado = false;
                }
                else {
                    alert_custom("S", "Produto excluído.");
                    deletado = true;
                }
            });
        }
    });
    sqlite.close();

    return deletado;
}