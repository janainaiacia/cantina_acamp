var update = false;
function getUserCod() {
    if ($("#iptIdUser").val() !== "") {
        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let sql = `SELECT * FROM USER WHERE USER_ID = ${$("#iptIdUser").val()}`;
        sqlite.run(sql, [], function (err) {
            if (err.error != undefined) {
                alert_custom("E", err.error);
            }
            else {
                if (err.length > 0) {
                    $("#iptIdUser").val(err[0].USER_ID);
                    $("#iptUsername").val(err[0].USER_USERNAME);
                    $("#iptSenha").val(err[0].USER_PASSWORD);

                    if (err[0].USER_ADM === "S")
                        $("#ckbAdm").prop("checked", true);
                    else
                        $("#ckbAdm").prop("checked", false);

                    if (err[0].USER_ACTIVE === "S")
                        $("#ckbAtivo").prop("checked", true);
                    else
                        $("#ckbAtivo").prop("checked", false);

                    update = true;
                }
                else {
                    $("#iptUsername").val("");
                    $("#iptSenha").val("");
                    $("#iptSenhaVer").val("");
                    $("#ckbAdm").prop("checked", false);
                    $("#ckbAtivo").prop("checked", true);

                    update = false;
                }
            }
        });
    }
    else {
        btnNovo_Click();
    }
}

function showPassword() {
    if ($("#iptSenhaVer").val() === '')
        $("#iptSenhaVer").val($("#iptSenha").val());
    else
        $("#iptSenhaVer").val("");
}

function btnNovo_Click() {
    $("#iptIdUser").val("");
    $("#iptUsername").val("");
    $("#iptSenha").val("");
    $("#iptSenhaVer").val("");
    $("#ckbAdm").prop("checked", false);
    $("#ckbAtivo").prop("checked", true);

    $("#iptIdUser").focus();
    update = false;
}

function btnSalvar_Click() {
    if (validaCampos()) {
        let sql = "";

        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let ativo = "N", adm = "N";

        if ($("#ckbAdm").is(":checked")) {
            adm = "S";
        }

        if ($("#ckbAtivo").is(":checked")) {
            ativo = "S";
        }

        if (update) { //Update
            sql = `UPDATE USER SET USER_USERNAME = '${$("#iptUsername").val().trim()}',
            USER_PASSWORD = '${$("#iptSenha").val().trim()}', USER_ACTIVE = '${ativo}',
            USER_ADM = '${adm}' WHERE USER_ID = ${$("#iptIdUser").val()}`;
        }
        else {
            if ($("#iptIdUser").val() !== "") {
                sql = `INSERT INTO USER (USER_ID, USER_USERNAME,  USER_PASSWORD, USER_ACTIVE, USER_ADM)
                VALUES (${$("#iptIdUser").val()}, '${$("#iptUsername").val().trim()}', '${$("#iptSenha").val().trim()}',
                '${ativo}', '${adm}')`;
            }
            else {
                sql = `INSERT INTO USER (USER_USERNAME,  USER_PASSWORD, USER_ACTIVE, USER_ADM)
                VALUES ('${$("#iptUsername").val().trim()}', '${$("#iptSenha").val().trim()}', 
                '${ativo}', '${adm}')`;
            }
        }
        sqlite.run(sql, [], function (err) {
            if (err.error != undefined) {
                alert_custom("E", err.error);
            }
            else
                alert_custom("S", "Usuário salvo.");
        });
    }
}

function validaCampos() {
    let msg = '';

    if ($("#iptUsername").val().trim() === "") {
        msg += "Informe o usuário." + "<br />";
    }
    else {
        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let sql = `SELECT * FROM USER WHERE upper(USER_USERNAME) = upper('${$("#iptUsername").val().trim()}')`;
        sqlite.run(sql, [], function (err) {
            if (err.error != undefined) {
                alert_custom("E", err.error);
                return false;
            }
            else {
                if (err.length > 0) {
                    if (update && err.length > 1)
                        msg += "Nome de usuário já existe." + "<br />";
                }
            }
        });
    }

    if ($("#iptSenha").val().trim() === "") {
        msg += "Informe a senha." + "<br />";
    }

    if (msg === "")
        return true;
    else {
        alert_custom("W", msg);
        return false;
    }
}


function getUserLikeName() {
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let sql = `SELECT * FROM USER WHERE upper(USER_USERNAME) LIKE
                upper('%${$("#iptUsernameC").val().trim()}%')`;

    $("#tbUser").find("tr:gt(0)").remove();
    let table = $("#tbUser")[0];

    sqlite.run(sql, [], function (err) {
        if (err.error != undefined) {
            alert_custom("E", err.error);
        }
        else {
            if (err.length > 0) {
                for (let i = 0; i < err.length; i++) {
                    let rowTable = table.insertRow(table.rows.length);

                    let cell1 = rowTable.insertCell(0);
                    let cell2 = rowTable.insertCell(1);
                    let cell3 = rowTable.insertCell(2);
                    let cell4 = rowTable.insertCell(3);
                    let cell5 = rowTable.insertCell(4);

                    cell1.innerHTML = err[i].USER_ID;
                    cell2.innerHTML = err[i].USER_USERNAME;


                    if (err[i].USER_ADM === "S") {
                        cell3.innerHTML = `<input type="checkbox" name="ckbAdmC" value="${err[i].USER_ID}" checked onchange="setCkbAdm(this);"> Administrador?`
                    } else {
                        cell3.innerHTML = `<input type="checkbox" name="ckbAdmC" value="${err[i].USER_ID}" onchange="setCkbAdm(this);"> Administrador?`
                    }

                    if (err[i].USER_ACTIVE === "S") {
                        cell4.innerHTML = `<input type="checkbox" name="ckbAtivoC" value="${err[i].USER_ID}" checked onchange="setCkbAtivoInativo(this);"> Ativo?`
                    } else {
                        cell4.innerHTML = `<input type="checkbox" name="ckbAtivoC" value="${err[i].USER_ID}" onchange="setCkbAtivoInativo(this);"> Ativo?`
                    }

                    cell5.innerHTML = `<button type='button' class='btn btn-sm'
                                            onclick='editUser(${err[i].USER_ID});'>
                                            <i class="fas fa-edit"></i>
                                    </button>`;

                    cell1.className = "text-center";
                    cell3.className = "text-center";
                    cell4.className = "text-center";
                    cell5.className = "text-center";
                }

            }
            else {
                alert_custom("W", "Usuário não encontrado");
            }
        }
    });
}

function setCkbAdm(ckb) {
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');
    let adm = "";
    if ($(ckb).is(":checked")) {
        adm = "S"
    } else {
        adm = "N";
    }

    sqlite.run(`UPDATE USER SET USER_ADM = '${adm}' WHERE USER_ID = ${ckb.value}`, [], function (err) {
        if (err.error != undefined) {
            alert_custom("E", err.error);
        }
        else {
            if (adm === "S")
                alert_custom("S", "Usuário alterado para administrador.");
            else
                alert_custom("S", "Usuário alterado para comum.");
        }
    });
}

function setCkbAtivoInativo(ckb) {
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');
    let ativo = "";
    if ($(ckb).is(":checked")) {
        ativo = "S"
    } else {
        ativo = "N";
    }

    sqlite.run(`UPDATE USER SET USER_ACTIVE = '${ativo}' WHERE USER_ID = ${ckb.value}`, [], function (err) {
        if (err.error != undefined) {
            alert_custom("E", err.error);
        }
        else {
            if (ativo === "S")
                alert_custom("S", "Usuário ativado.");
            else
                alert_custom("S", "Usuário desativado.");
        }
    });
}

function editUser(id) {
    $("#iptIdUser").val(id);
    getUserCod();
    changeNav('a1', 'form-cadastro');
}

function deleteUser(id, btn) {

    let deletado = deleteUserId(id);
    if (deletado) {
        let tr_delete = $(btn).closest('tr');
        tr_delete.remove();
        btnNovo_Click();
    }
    else {
        let ckb = tr_delete.find("ckbAtivoC");
        ckb.prop("checked", false);
    }
}

function deleteUserId(id) {

    if (sessionStorage.getItem("user_id") == id) {
        alert_custom("W", "Não é possível excluir o usuário logado.");
        return false;
    }
    else {
        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let deletado = true;

        let sql = `DELETE FROM USER WHERE USER_ID = ${id}`;
        sqlite.run(sql, [], function (err) {
            if (err.error != undefined) {

                sql = `UPDATE USER SET USER_ACTIVE = 'N' WHERE USER_ID = ${id}`;
                sqlite.run(sql, [], function (err) {
                    if (err.error != undefined) {
                        alert_custom("E", err.error);
                        deletado = false;
                    }
                    else {
                        alert_custom("W", "Não foi possível excluir o usuário. O usuário foi desativado.");
                        deletado = false;
                    }
                });
            }
            else {
                alert_custom("S", "Usuário excluído.");
                btnNovo_Click();
                deletado = true;
            }
        });

        return deletado;
    }
}