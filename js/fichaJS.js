$(document).ready(function () {
    $("#iptValor").maskMoney();
});

function getAcampantes() {
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let sql = `SELECT * FROM ACAMPANTE WHERE upper(ACAMP_NAME) LIKE upper('%${$("#iptNameAcampM").val()}%');`;

    let row = sqlite.run(sql);

    if (row.length > 0) {
        $("#tbAcamp").find("tr:gt(0)").remove();
        let table = $("#tbAcamp")[0];

        for (let i = 0; i < row.length; i++) {
            let rowTable = table.insertRow(table.rows.length);

            let cell1 = rowTable.insertCell(0);
            let cell2 = rowTable.insertCell(1);
            let cell3 = rowTable.insertCell(2);
            let cell4 = rowTable.insertCell(3);

            cell1.innerHTML = `<button type='button' class='btn btn-sm btn-link' onclick='selectAcamp(${row[i].ACAMP_ID});'>${row[i].ACAMP_ID}</button>`;
            cell2.innerHTML = `<button type='button' class='btn btn-sm btn-link' onclick='selectAcamp(${row[i].ACAMP_ID});'>${row[i].ACAMP_NAME}</button>`;
            cell3.innerHTML = row[i].ACAMP_DT_NASC;
            cell4.innerHTML = row[i].ACAMP_FONE;

            cell1.className = "text-center";
            cell3.className = "text-center";
            cell4.className = "text-center";
        }
    }
    else {
        alert_custom("W", "Acampante não encontrado.");
    }
}

function selectAcamp(id) {
    $("#iptIdAcamp").val(id);
    $('#ModalPesqAcamp').modal('hide');
    getNameAcamp_Blur();
}

function getNameAcamp_Blur() {
    if ($("#iptIdAcamp").val() !== "") {

        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');
        let sql = `SELECT * FROM ACAMPANTE WHERE ACAMP_ID = ${$("#iptIdAcamp").val()}`;
        let row = sqlite.run(sql);

        if (row.length > 0) {
            $("#iptNameAcamp").val(row[0].ACAMP_NAME);
        }
        else {
            $("#iptNameAcamp").val("");
        }
    }
    else {
        $("#iptIdAcamp").val("");
        $("#iptNameAcamp").val("");
    }
}

function btnNovo_Click() {
    $("#iptIdAcamp").val("");
    $("#iptNameAcamp").val("");
    $("#iptValor").val("");
}

function btnSalvar_Click() {
    if (validaCampos()) {

        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        sqlite.run("BEGIN TRANSACTION", [], function (err) {
            if (err.error != undefined) {
                alert_custom("E", err.error);
                sqlite.run("ROLLBACK");
            }
            else {
                let sql = `SELECT (F.FICHA_VALOR - (SELECT coalesce(SUM(V.VEN_TOTAL),0) FROM VENDAS V 
                INNER JOIN ACAMPANTE_FICHA F ON V.FICHA_ID = F.FICHA_ID AND V.ACAMP_ID = F.ACAMP_ID AND FICHA_ATIVA='S'
                AND V.ACAMP_ID = ${$("#iptIdAcamp").val()})) AS FICHA_VALOR 
                FROM ACAMPANTE_FICHA F WHERE F.ACAMP_ID = ${$("#iptIdAcamp").val()} AND FICHA_ATIVA='S' `;
                sqlite.run(sql, [], function (err) {
                    if (err.error != undefined) {
                        alert_custom("E", err.error);
                        sqlite.run("ROLLBACK");
                    } else {
                        let valor = 0;
                        if (err.length > 0) {//se tiver ficha aberta
                            valor = parseFloat(err[0].FICHA_VALOR);
                            sql = `UPDATE ACAMPANTE_FICHA SET FICHA_ATIVA = 'N' WHERE ACAMP_ID = ${$("#iptIdAcamp").val()}`;
                            sqlite.run(sql, [], function (err) {
                                if (err.error != undefined) {
                                    alert_custom("E", err.error);
                                    sqlite.run("ROLLBACK");
                                    return false;
                                }
                            });
                        }
                        valor += parseFloat($("#iptValor").val().replace(',', '.'));

                        sql = `INSERT INTO ACAMPANTE_FICHA (ACAMP_ID, FICHA_VALOR, FICHA_ATIVA) 
                                VALUES (${$("#iptIdAcamp").val()}, ${valor}, 'S');`;

                        sqlite.run(sql, [], function (err) {
                            if (err.error != undefined) {
                                alert_custom("E", err.error);
                                sqlite.run("ROLLBACK");
                                return false;
                            }
                            else {
                                sqlite.run("COMMIT");
                                alert_custom("S", "Ficha Salva.");
                                btnNovo_Click();
                            }
                        });
                    }
                });
            }
        });
    }
}

function validaCampos() {
    let msg = "";
    if ($("#iptIdAcamp").val() === "" || $("#iptNameAcamp").val() === "") {
        msg += "Informe o acampante." + "<br />";
    }

    if ($("#iptValor").val() === "" || parseFloat($("#iptValor").val()) === 0) {
        msg += "Informe o valor da ficha." + "<br />";
    }

    if (msg === "") {
        return true;
    }
    else {
        alert_custom("W", msg);
        return false;
    }
}

function btnConsulta_Click() {
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    $("#tbFichas").find("tr:gt(0)").remove();

    let sql = `SELECT F.FICHA_ID, A.ACAMP_NAME, 
    printf("%.2f", F.FICHA_VALOR) AS FICHA_VALOR, 
	printf("%.2f", coalesce( (SELECT sum (V.VEN_TOTAL) FROM VENDAS V 
    WHERE  V.ACAMP_ID = A.ACAMP_ID AND V.FICHA_ID = F.FICHA_ID), 0) ) AS VALOR_GASTO,
	printf("%.2f",coalesce((SELECT SUM(R.RET_VALOR) FROM ACAMPANTE_FICHA_RETIRADA R WHERE  
    R.ACAMP_ID = A.ACAMP_ID AND R.FICHA_ID = F.FICHA_ID), 0)) AS VALOR_RET,
    printf("%.2f", F.FICHA_VALOR - coalesce( (SELECT sum (V.VEN_TOTAL) FROM VENDAS V 
    WHERE  V.ACAMP_ID = A.ACAMP_ID AND V.FICHA_ID = F.FICHA_ID), 0) - 
    coalesce((SELECT SUM(R.RET_VALOR) FROM ACAMPANTE_FICHA_RETIRADA R WHERE  
    R.ACAMP_ID = A.ACAMP_ID AND R.FICHA_ID = F.FICHA_ID), 0)) AS VALOR_REST
    FROM ACAMPANTE_FICHA F, ACAMPANTE A
    WHERE F.ACAMP_ID = A.ACAMP_ID AND 
    upper(A.ACAMP_NAME)  LIKE upper('%${$("#iptNameAcampC").val()}%') 
    AND F.FICHA_ATIVA='S'
    ORDER BY A.ACAMP_NAME, F.FICHA_ID`;

    sqlite.run(sql, [], function (err) {
        if (err.error != undefined) {
            alert_custom("E", err.error);
        }
        else {
            if (err.length > 0) {
                let table = $("#tbFichas")[0];

                for (let i = 0; i < err.length; i++) {
                    let rowTable = table.insertRow(table.rows.length);

                    let cell1 = rowTable.insertCell(0);
                    let cell2 = rowTable.insertCell(1);
                    let cell3 = rowTable.insertCell(2);
                    let cell4 = rowTable.insertCell(3);
                    let cell5 = rowTable.insertCell(4);
                    let cell6 = rowTable.insertCell(5);

                    cell1.innerHTML = err[i].FICHA_ID;
                    cell2.innerHTML = err[i].ACAMP_NAME;
                    cell3.innerHTML = err[i].FICHA_VALOR;
                    cell4.innerHTML = err[i].VALOR_GASTO;
                    cell5.innerHTML = err[i].VALOR_RET;
                    cell6.innerHTML = err[i].VALOR_REST;

                    cell1.className='text-center';
                    cell3.className='text-center';
                    cell4.className='text-center';
                    cell5.className='text-center';
                    cell6.className='text-center';
                }
            } else {
                alert_custom("W", "Nenhuma ficha encontrada.");
            }
        }
    });
}