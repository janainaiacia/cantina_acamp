﻿function getDateElement(element, dateFormat) {
    var date;
    try {
        date = $.datepicker.parseDate(dateFormat, (element.value || element.val()));
    } catch (error) {
        date = null;
    }
    return date;
}

function PairDateConfig(idFrom, idTo) {
    var dateFormat = "dd/mm/yy",
        from = $(`#${idFrom}`)
            .datepicker()
            .on("change", function () {
                to.datepicker("option", "minDate", getDateElement(this, dateFormat));
            }),
        to = $(`#${idTo}`)
            .datepicker()
            .on("change", function () {
                from.datepicker("option", "maxDate", getDateElement(this, dateFormat));
            });
    if (from.val() !== "")
        to.datepicker("option", "minDate", getDateElement(from, dateFormat));
    if (to.val() !== "")
        from.datepicker("option", "maxDate", getDateElement(to, dateFormat));
}

function SingleDateConfig(idField) {
    $(`#${idField}`).datepicker();
}