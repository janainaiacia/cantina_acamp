function getNameAcamp_Blur() {

    if ($("#iptIdAcamp").val() !== "") {

        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');
        let sql = `SELECT  A.ACAMP_NAME, 
        coalesce(F.FICHA_VALOR - coalesce(SUM(V.VEN_TOTAL), 0), 0) AS VALOR_REST,
        F.FICHA_ID  
        FROM ACAMPANTE A
        INNER JOIN ACAMPANTE_FICHA F ON F.ACAMP_ID = A.ACAMP_ID AND F.FICHA_ATIVA = 'S'
        LEFT JOIN VENDAS V ON A.ACAMP_ID = V.ACAMP_ID AND F.FICHA_ID = V.FICHA_ID
        WHERE A.ACAMP_ID = ${$("#iptIdAcamp").val()}
        ORDER BY A.ACAMP_NAME, F.FICHA_ID`;
        sqlite.run(sql, [], function (err) {
            if (err.length > 0) {
                $("#iptNameAcamp").val(err[0].ACAMP_NAME);
                $("#iptFicha").val(number_format(err[0].VALOR_REST));
                $("#iptValorDevolucao").val(number_format(err[0].VALOR_REST));

                $("#iptValorDevolucao").focus();
            }
            else {
                $("#iptNameAcamp").val("");
                $("#iptFicha").val("");
                $("#iptValorDevolucao").val("");
            }
        });
    }
    else {
        $("#iptIdAcamp").val("");
        $("#iptNameAcamp").val("");
        $("#iptFicha").val("");
        $("#iptValorDevolucao").val("");
    }
}

function limpa_campos() {
    $("#iptIdAcamp").val("");
    $("#iptNameAcamp").val("");
    $("#iptFicha").val("");
    $("#iptValorDevolucao").val("");
    $("#iptIdAcamp").focus();
}

function getAcampantes() {
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let sql = `SELECT * FROM ACAMPANTE WHERE upper(ACAMP_NAME) LIKE upper('%${$("#iptNameAcampM").val()}%');`;

    let row = sqlite.run(sql);

    if (row.length > 0) {
        $("#tbAcamp").find("tr:gt(0)").remove();
        let table = $("#tbAcamp")[0];

        for (let i = 0; i < row.length; i++) {
            let rowTable = table.insertRow(table.rows.length);

            let cell1 = rowTable.insertCell(0);
            let cell2 = rowTable.insertCell(1);
            let cell3 = rowTable.insertCell(2);
            let cell4 = rowTable.insertCell(3);

            cell1.innerHTML = `<button type='button' class='btn btn-sm btn-link' onclick='selectAcamp(${row[i].ACAMP_ID});'>${row[i].ACAMP_ID}</button>`;
            cell2.innerHTML = `<button type='button' class='btn btn-sm btn-link' onclick='selectAcamp(${row[i].ACAMP_ID});'>${row[i].ACAMP_NAME}</button>`;
            cell3.innerHTML = row[i].ACAMP_DT_NASC;
            cell4.innerHTML = row[i].ACAMP_FONE;

            cell1.className = "text-center";
            cell3.className = "text-center";
            cell4.className = "text-center";
        }
    }
    else {
        alert_custom("W", "Acampante não encontrado.");
    }
}

function selectAcamp(id) {
    $("#iptIdAcamp").val(id);
    $('#ModalPesqAcamp').modal('hide');
    getNameAcamp_Blur();
}

$(document).ready(function () {
    $("#iptValorDevolucao").maskMoney();
});

function btnDevolver_Click() {

    if (validaCampos()) {

        let ficha = $("#iptFicha").val().replaceAll(".", "").replaceAll(",", ".");
        let valor_devolvido = $("#iptValorDevolucao").val().replaceAll(".", "").replaceAll(",", ".");

        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let sql = `SELECT AF.FICHA_ID, AF.FICHA_VALOR, (SELECT SUM(V.VEN_TOTAL) FROM VENDAS V
        WHERE V.FICHA_ID = AF.FICHA_ID) AS VEN_TOTAL 
        FROM ACAMPANTE_FICHA AF WHERE ACAMP_ID = ${$("#iptIdAcamp").val()} AND FICHA_ATIVA = 'S'`;

        let value = sqlite.run(sql);        
        let ficha_id = value[0].FICHA_ID;
        let ven_total = value[0].VEN_TOTAL;
        let ficha_valor = parseFloat(value[0].FICHA_VALOR) - valor_devolvido - ven_total;

        sql = `INSERT INTO ACAMPANTE_FICHA_RETIRADA (RET_VALOR, USER_ID, ACAMP_ID, FICHA_ID)
                    VALUES (${valor_devolvido}, ${sessionStorage.getItem("user_id")}, ${$("#iptIdAcamp").val()}, ${ficha_id})`;

        sqlite.run(sql, [], function (err) {
            if (err.error != undefined) {
                alert_custom("E", err.error);
            }
            else {

                sql = `UPDATE ACAMPANTE_FICHA SET FICHA_ATIVA = 'N'
                WHERE FICHA_ID = ${ficha_id}`;

                sqlite.run(sql, [], function (err) {
                    if (err.error != undefined) {
                        alert_custom("E", err.error);
                    }
                    else {

                        if (ficha_valor > 0) {
                            sql = `INSERT INTO ACAMPANTE_FICHA (ACAMP_ID, FICHA_VALOR, FICHA_ATIVA)
                            VALUES (${$("#iptIdAcamp").val()}, ${ficha_valor}, 'S' )`;

                            sqlite.run(sql, [], function (err) {
                                if (err.error != undefined) {
                                    alert_custom("E", err.error);
                                }
                                else {
                                    alert_custom("S", "Devolução realizada.");
                                    limpa_campos();
                                }
                            });
                        }
                        else {
                            alert_custom("S", "Devolução realizada.");
                            limpa_campos();
                        }
                    }
                });
            }
        });
    }
}

function validaCampos() {
    let msg = '';

    if ($("#iptIdAcamp").val() === "" || $("#iptNameAcamp").val() === "") {
        msg += "Informe o acampante.";
    }
    else {
        if ($("#iptFicha").val() !== "" && $("#iptFicha").val() !== "0,00") {
            if ($("#iptValorDevolucao").val() !== "" && $("#iptValorDevolucao").val() !== "0,00") {
                let ficha = $("#iptFicha").val().replaceAll(".", "").replaceAll(",", ".");
                let valor_devolvido = $("#iptValorDevolucao").val().replaceAll(".", "").replaceAll(",", ".");

                if (ficha < valor_devolvido) {
                    msg += "Valor maior do que o da ficha.";
                }
            }
            else {
                msg += "Informe o valor devolvido.";
            }
        }
        else {
            msg += "Não há valor para devolução.";
        }
    }

    if (msg === "") {
        return true;
    }
    else {
        alert_custom("W", msg);
        return false;
    }
}