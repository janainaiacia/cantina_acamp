function MaskSomenteNumeros(e) {
    if (!$.browser) {
        $.browser = {};
        $.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase());
        $.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
        $.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
        $.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());
        $.browser.device = /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase());
    }

    e = e || window.event;
    var key = e.which || e.charCode || e.keyCode,
        home = 36,
        end = 35,
        backspace = 8,
        del = 46,
        leftArrow = 37,
        rightArrow = 39,
        enter = 13,
        tab = 9;
    //added to handle an IE "special" event
    if (key === undefined) {
        return false;
    }
    if ((key < 48 || key > 57) && (key !== enter) && key !== tab) {
        if ($.browser.mozilla && (key === home || key === backspace || (key === del && e.key !== '.') || key === end || ((key === leftArrow || key === rightArrow) && e.charCode === 0)))
            return true;
        else
            return false;
    }
    else {
        return true;
    }
}

function MaskDataHora(objeto, e) {
	var retorno = false;
	//47  =     / 
	//58  =     :  
	//32  = Space Bar

	separador = '/';
	separador2 = ' ';
	separador3 = ':';

	evt = e || window.event;
	var tecla = evt.which || evt.keyCode;
	
	if (tecla == 8 /*backspace*/ || tecla == 9/**/ || tecla == 46/*delete*/
	 || tecla == 37/*seta esquerda*/ || tecla == 39/*seta direita*/)
		return true;
	else {

		if ((tecla >= 48) && (tecla <= 57)) {
			var tam = 16;

			conjunto1 = 2;
			conjunto2 = 5;
			conjunto3 = 10;
			conjunto4 = 13;

			if (objeto.value.length == conjunto1) {
				objeto.value = objeto.value + separador;
				retorno = true;
			}
			if (objeto.value.length == conjunto2) {
				objeto.value = objeto.value + separador;
				retorno = true;
			}
			if (objeto.value.length == conjunto3) {
				objeto.value = objeto.value + separador2;
				retorno = true;
			}
			if (objeto.value.length == conjunto4) {
				objeto.value = objeto.value + separador3;
				returno = true;
			}
			else {
				var pos = GetPosCarroTextBox(objeto);
				var range = objeto.createTextRange();
				range.collapse(true);

				if ((pos == conjunto1) || (pos == conjunto2) || (pos == conjunto3) ||
					(pos == conjunto4) || (pos == conjunto5)) {
					range.moveStart('character', pos + 1);
					range.moveEnd('character', 1);
					range.select();
					retorno = true;
				}
				else if (pos < tam) {
					if ((objeto.value.substr(pos, 1) == separador) ||
						(objeto.value.substr(pos, 1) == separador2) ||
						(objeto.value.substr(pos, 1) == separador3)
						) {
						range.moveStart('character', pos);
						range.moveEnd('character', 0);
						range.select();

					}
					else {
						range.moveStart('character', pos);
						range.moveEnd('character', 1);
						range.select();
					}

					retorno = true;
				}
				else retorno = false;
			}
		}
	}
	return retorno;
};

function MaskData(objeto, e)
{
	evt = e || window.event;
	var tecla = evt.which || evt.keyCode;

	if (tecla == 8 /*backspace*/ || tecla == 9/**/ || tecla == 46/*delete*/
	 || tecla == 37/*seta esquerda*/ || tecla == 39/*seta direita*/)
		return true;
	else {

		if (tecla >= 48 && tecla <= 57) {
			separador = '/';
			conjunto1 = 2;
			conjunto2 = 5;
			if (objeto.value.length == conjunto1)
				objeto.value = objeto.value + separador;
			if (objeto.value.length == conjunto2)
				objeto.value = objeto.value + separador;
			return true;
		}
	}
	return false;
};

function MaskHora(objeto, e)
{
	evt = e || window.event;
	var tecla = evt.which || evt.keyCode;

	if (tecla == 8 /*backspace*/ || tecla == 9/**/ || tecla == 46/*delete*/
	 || tecla == 37/*seta esquerda*/ || tecla == 39/*seta direita*/)
		return true;

	else
		{
			separador = ':';
			conjunto1 = 2;
			if (objeto.value.length == conjunto1)
				objeto.value = objeto.value + separador;
			return true;
		}
	return false;
};

function alert_custom(type, message){

	try{
		$("#modalAlert").remove();
	}catch{}
    /*
    Type
    S = Sucesso
    E = Erro
    W = Alerta
    */

    let modal = `<!-- Modal -->
    <div class="modal fade" id="modalAlert" tabindex="-1" role="dialog" aria-labelledby="modal_alert_label" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div style="width:100%; text-align:center">`;
            if(type === 'S')//sucess
                modal += `<i class="fa fa-6x fa-check-circle" style="color:#47c9a2"></i>`;
            else if(type === "E")//erro
                modal += `<i class="fa fa-6x fa-times-circle" style="color:#f8d7da"></i>`;
            else if(type === "W")//Alerta
                modal += `<i class="fas fa-6x fa-exclamation-circle" style="color:#ffeeba"></i>`;

            modal += `<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
          </div>
          <div class="modal-body">`;
                if(type === 'S')//sucess
                modal += `<div class="alert alert-success" role="alert" style="text-align:center;">`;
                else if(type === "E")//erro
                    modal += `<div class="alert alert-danger" role="alert" style="text-align:center;">`;
                else if(type === "W")//Alerta
                    modal += `<div class="alert alert-warning" role="alert" style="text-align:center;">`;
          
            modal += `${message}</div>
        </div>
      </div>
    </div>`;

    $("body").append(modal);

    $("#modalAlert").modal('show');
}

function number_format(number, decimals, dec_point, thousands_sep) {
    var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
    var d = dec_point == undefined ? "," : dec_point;
    var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
    var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}


String.prototype.replaceAll = function (de, para) {
    var str = this;
    var pos = str.indexOf(de);
    while (pos > -1) {
        str = str.replace(de, para);
        pos = str.indexOf(de);
    }
    return (str);
}

function FormataStringData(data) {
    var dia = data.split("/")[0];
    var mes = data.split("/")[1];
    var ano = data.split("/")[2];

    return ano + '-' + ("0" + mes).slice(-2) + '-' + ("0" + dia).slice(-2);
    // Utilizo o .slice(-2) para garantir o formato com 2 digitos.
}