/******************RELATÓRIO******************/
$(document).ready(function () {
    $("#iptDtIni").datepicker();
    $("#iptDtFim").datepicker();

    var d = new Date();

    var month = d.getMonth()+1;
    var day = d.getDate();

    var output = (day<10 ? '0' : '') + day + '/' +
    (month<10 ? '0' : '') + month + '/' +
    d.getFullYear() ;

    $("#iptDtIni").val(output);
    $("#iptDtFim").val(output);
});


function btnBuscar_Click() {
    if ($("#iptDtIni").val() !== "" && $("#iptDtFim").val() !== "") {
        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let sql = `SELECT V.VEN_ID, strftime('%d/%m/%Y',V.VEN_DT_REG) AS VEN_DT_REG,
        CASE WHEN V.ACAMP_ID IS NULL THEN 'VISITANTE' ELSE A.ACAMP_NAME
        END AS ACAMP_NAME, CASE WHEN V.VEN_TP_PAGTO = 'D' THEN
        'DINHEIRO' WHEN V.VEN_TP_PAGTO = 'C' THEN  'CARTÃO'
        ELSE 'FICHA'END AS VEN_TP_PAGTO, V.VEN_TOTAL, U.USER_USERNAME
        FROM VENDAS V INNER JOIN USER U ON U.USER_ID = V.USER_ID
        LEFT JOIN ACAMPANTE A ON A.ACAMP_ID = V.ACAMP_ID
        WHERE date(V.VEN_DT_REG) BETWEEN 
        date('${FormataStringData($("#iptDtIni").val())}') 
        AND date('${FormataStringData($("#iptDtFim").val())}')`

        sqlite.run(sql, [], function (err) {
            if (err.error != undefined) {
                alert_custom("E", err.error);
            } else {
                if (err.length > 0) {
                    $("#tbRelatorio").find("tr:gt(0)").remove();
                    let table = $("#tbRelatorio")[0];
                    $("#div-relatorio").show();
                    for (let i = 0; i < err.length; i++) {
                        let rowTable = table.insertRow(table.rows.length);

                        let cell1 = rowTable.insertCell(0);
                        let cell2 = rowTable.insertCell(1);
                        let cell3 = rowTable.insertCell(2);
                        let cell4 = rowTable.insertCell(3);
                        let cell5 = rowTable.insertCell(4);
                        let cell6 = rowTable.insertCell(5);

                        cell1.innerHTML = `<button type='button' class='btn btn-sm btn-outline-secondary' onclick='selectPlus(${err[i].VEN_ID}, this);'>
                                                <i class="fas fa-angle-down"></i>
                                            </button>`;
                        cell2.innerHTML = err[i].VEN_DT_REG;
                        cell3.innerHTML = err[i].ACAMP_NAME;
                        cell4.innerHTML = err[i].VEN_TP_PAGTO;
                        cell5.innerHTML = number_format(err[i].VEN_TOTAL, 2, ',', '.');
                        cell6.innerHTML = err[i].USER_USERNAME;

                        cell1.className = "text-center";
                        cell2.className = "text-center";
                        cell4.className = "text-center";
                        cell5.className = "text-center";

                        createTableProducts(err[i].VEN_ID);
                    }
                }
                else {
                    alert_custom("W", "Nenhum venda encontrada");
                    $("#div-relatorio").hide();
                }
            }
        });
    }
    else {
        alert_custom("W", "Preencha as datas para a pesquisa. ")
    }
}

function createTableProducts(ven_id) {
    let colspan = 6;

    let table = $("#tbRelatorio")[0];
    let rowTable = table.insertRow(table.rows.length);

    let cell = rowTable.insertCell(0);
    cell.colSpan = colspan;

    cell.innerHTML = `<table class='table table-sm' style='width:80%; display:none; margin:0 auto;' id='tbProds_${ven_id}'>
                        <thead class='thead-dark'>
                            <tr>
                                <th>Produto</th>
                                <th class='text-center'>Quantidade</th>
                                <th class='text-center'>Valor Total</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                     </table>`;

    table = $(`#tbProds_${ven_id}`)[0];

    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let sql = `SELECT P.PRO_ID, P.PRO_DESCRIPTION, V.PRO_QTDE, printf("%.2f", V.PRO_TOTAL) AS PRO_TOTAL
    FROM PRODUTOS P, VENDAS_PRODUTOS V
    WHERE V.PRO_ID = P.PRO_ID AND V.VEN_ID = ${ven_id}`;

    sqlite.run(sql, [], function (err) {
        if (err.error != undefined) {
            alert_custom("E", err.error);
        }
        else {

            if (err.length > 0) {
                let rowTable, cell1, cell2, cell3, cell4;
                for (let i = 0; i < err.length; i++) {
                    rowTable = table.insertRow(table.rows.length);
                    cell1 = rowTable.insertCell(0);
                    cell2 = rowTable.insertCell(1);
                    cell3 = rowTable.insertCell(2);

                    cell1.innerHTML = err[i].PRO_DESCRIPTION;
                    cell2.innerHTML = err[i].PRO_QTDE;
                    cell3.innerHTML = number_format(err[i].PRO_TOTAL, 2, ',', '.');

                    cell2.className = "text-center";
                    cell3.className = "text-center";
                }
            }
            else {
                alert_custom("W", "Nenhum produto da venda selecionada foi encontrado.");
            }
        }
    });

    sqlite.close();
}

function selectPlus(ven_id, btn) {

    let obj_i = $(btn).find("i");
    if (obj_i[0].className.indexOf("down") > -1) {//abrir
        obj_i[0].className = "fa fa-angle-up";
        $("#tbProds_" + ven_id).show();
    }
    else {
        obj_i[0].className = "fa fa-angle-down";
        $("#tbProds_" + ven_id).hide();
    }
}