function getNameAcamp_Blur() {
    if ($("#iptIdAcamp").val() !== "") {

        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');
        let sql = `SELECT  A.ACAMP_NAME
        FROM ACAMPANTE A
         WHERE A.ACAMP_ID = ${$("#iptIdAcamp").val()}`;
        let row = sqlite.run(sql);

        if (row.length > 0) {
            $("#iptNameAcamp").val(row[0].ACAMP_NAME);
        }
        else {
            $("#iptNameAcamp").val("");
            alert_custom("W", "Acampante não encontrado.");
        }
    }
    else {
        $("#iptIdAcamp").val("");
        $("#iptNameAcamp").val("");
    }
}

function btnBuscar_Click() {
    let sql = "", sql_produtos = "";

    $("#tbVendas").html("");
    let table = $("#tbVendas")[0];
    let type;
    if ($("#rbAcampante").is(":checked")) {
        if ($("#iptIdAcamp").val() === "" || $("#iptNameAcamp").val() === "") {
            alert_custom("W", "Informe o acampante.");
            return false;
        }
        else {
            sql = `SELECT A.ACAMP_ID, A.ACAMP_NAME, V.VEN_ID, strftime('%d/%m/%Y',V.VEN_DT_REG) AS VEN_DT_REG,
            printf("%.2f", V.VEN_TOTAL) AS VEN_TOTAL FROM VENDAS V, ACAMPANTE A WHERE A.ACAMP_ID = V.ACAMP_ID
            AND A.ACAMP_ID = ${$("#iptIdAcamp").val()} ORDER BY V.VEN_DT_REG`;
            type = "A";
        }
    } else {
        sql = `SELECT 'VISITANTE' AS ACAMP_NAME, V.VEN_ID, strftime('%d/%m/%Y',V.VEN_DT_REG) AS VEN_DT_REG,
        CASE WHEN V.VEN_TP_PAGTO = 'D' THEN 'DINEHIRO' ELSE 'CARTÃO' END AS VEN_TP_PAGTO,
        printf("%.2f", V.VEN_TOTAL) AS VEN_TOTAL FROM VENDAS V WHERE V.VEN_VISITANTE = 'S'`;
        type = "V";
    }

    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    sqlite.run(sql, [], function (err) {
        if (err.error != undefined) {
            alert_custom("E", err.error);
        } else {
            if (err.length > 0) {
                $("#div-result").show();
                for (let i = 0; i < err.length; i++) {
                    var rowTable;
                    if (type === "A") {
                        if (i === 0) {
                            var header = table.createTHead();

                            // Create an empty <tr> element and add it to the first position of <thead>:
                            rowTable = header.insertRow(0);

                            var cell1 = rowTable.insertCell(0);
                            var cell2 = rowTable.insertCell(1);
                            var cell3 = rowTable.insertCell(2);
                            var cell4 = rowTable.insertCell(3);

                            cell1.innerHTML = "";
                            cell2.innerHTML = "<b>Nome Acampante</b>";
                            cell3.innerHTML = "<b>Data Venda</b>";
                            cell4.innerHTML = "<b>Valor total Venda</b>";

                            cell1.className = "text-center";
                            cell3.className = "text-center";
                            cell4.className = "text-center";
                        }

                        rowTable = table.insertRow(table.rows.length);

                        var cell1 = rowTable.insertCell(0);
                        var cell2 = rowTable.insertCell(1);
                        var cell3 = rowTable.insertCell(2);
                        var cell4 = rowTable.insertCell(3);

                        cell1.innerHTML = `<button type='button' class='btn btn-sm btn-outline-secondary' onclick='selectPlus(${err[i].VEN_ID}, this);'>
                                                <i class="fas fa-angle-down"></i>
                                            </button>`;
                        cell2.innerHTML = err[i].ACAMP_NAME;
                        cell3.innerHTML = err[i].VEN_DT_REG;
                        cell4.innerHTML = number_format(err[i].VEN_TOTAL, 2, ',', '.');

                        cell1.className = "text-center";
                        cell3.className = "text-center";
                        cell4.className = "text-center";
                    }
                    else {

                        if (i === 0) {
                            var header = table.createTHead();

                            // Create an empty <tr> element and add it to the first position of <thead>:
                            rowTable = header.insertRow(0);

                            var cell1 = rowTable.insertCell(0);
                            var cell2 = rowTable.insertCell(1);
                            var cell3 = rowTable.insertCell(2);
                            var cell4 = rowTable.insertCell(3);
                            var cell5 = rowTable.insertCell(4);

                            cell1.innerHTML = "";
                            cell2.innerHTML = "";
                            cell3.innerHTML = "<b>Tipo Pagto</b>";
                            cell4.innerHTML = "<b>Data Venda</b>";
                            cell5.innerHTML = "<b>Valor total Venda</b>";

                            cell1.className = "text-center";
                            cell3.className = "text-center";
                            cell4.className = "text-center";
                            cell5.className = "text-center";
                        }

                        rowTable = table.insertRow(table.rows.length);

                        var cell1 = rowTable.insertCell(0);
                        var cell2 = rowTable.insertCell(1);
                        var cell3 = rowTable.insertCell(2);
                        var cell4 = rowTable.insertCell(3);
                        var cell5 = rowTable.insertCell(4);

                        cell1.innerHTML = `<button type='button' class='btn btn-sm btn-outline-secondary' onclick='selectPlus(${err[i].VEN_ID}, this);'>
                                                <i class="fas fa-angle-down"></i>
                                            </button>`;
                        cell2.innerHTML = err[i].ACAMP_NAME;
                        cell3.innerHTML = err[i].VEN_TP_PAGTO;
                        cell4.innerHTML = err[i].VEN_DT_REG;
                        cell5.innerHTML = number_format(err[i].VEN_TOTAL, 2, ',', '.');

                        cell1.className = "text-center";
                        cell3.className = "text-center";
                        cell4.className = "text-center";
                        cell5.className = "text-center";
                    }
                    rowTable.id = `tr_${err[i].VEN_ID}`;
                    createTableProducts(err[i].VEN_ID);
                }
            }
            else {
                $("#div-result").hide();
                alert_custom("W", "Nenhuma venda encontrada.");
            }
        }
    });
}

function onChageFiltro() {
    if ($("input[name='radioPesq']:checked").val() === 'V') {
        $("#iptIdAcamp").prop("disabled", true);
        $("#btnPesqAcamp").prop("disabled", true);
    }
    else {//Acampante
        $("#iptIdAcamp").prop("disabled", false);
        $("#btnPesqAcamp").prop("disabled", false);
    }
}

function selectPlus(ven_id, btn) {

    let obj_i = $(btn).find("i");
    if (obj_i[0].className.indexOf("down") > -1) {//abrir
        obj_i[0].className = "fa fa-angle-up";
        $("#tbProds_" + ven_id).show();
    }
    else {
        obj_i[0].className = "fa fa-angle-down";
        $("#tbProds_" + ven_id).hide();
    }
}

function createTableProducts(ven_id) {
    let colspan = 0;
    if ($("input[name='radioPesq']:checked").val() === 'V') {
        colspan = 5;
    }
    else {
        colspan = 4;
    }

    let table = $("#tbVendas")[0];
    let rowTable = table.insertRow(table.rows.length);

    let cell = rowTable.insertCell(0);
    cell.colSpan = colspan;

    cell.innerHTML = `<table class='table table-sm' style='width:100%; display:none;' id='tbProds_${ven_id}'>
                        <thead>
                            <tr>
                                <th>Produto</th>
                                <th class='text-center'>Quantidade</th>
                                <th class='text-center'>Valor Total</th>
                                <th class='text-center'>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                     </table>`;

    table = $(`#tbProds_${ven_id}`)[0];

    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let sql = `SELECT P.PRO_ID, P.PRO_DESCRIPTION, V.PRO_QTDE, printf("%.2f", V.PRO_TOTAL) AS PRO_TOTAL
    FROM PRODUTOS P, VENDAS_PRODUTOS V
    WHERE V.PRO_ID = P.PRO_ID AND V.VEN_ID = ${ven_id}`;

    sqlite.run(sql, [], function (err) {
        if (err.error != undefined) {
            alert_custom("E", err.error);
        }
        else {

            if (err.length > 0) {
                let rowTable, cell1, cell2, cell3, cell4;
                for (let i = 0; i < err.length; i++) {
                    rowTable = table.insertRow(table.rows.length);
                    cell1 = rowTable.insertCell(0);
                    cell2 = rowTable.insertCell(1);
                    cell3 = rowTable.insertCell(2);
                    cell4 = rowTable.insertCell(3);

                    cell1.innerHTML = err[i].PRO_DESCRIPTION;
                    cell2.innerHTML = err[i].PRO_QTDE;
                    cell3.innerHTML = number_format(err[i].PRO_TOTAL, 2, ',', '.');
                    cell4.innerHTML = `<button type='button' class='btn btn-sm' style='color:red;' onclick='estorno_click(${ven_id}, ${err[i].PRO_ID}, this);'>
                                            <i class="fas fa-times"></i>
                                        </button>`;

                    cell2.className = "text-center";
                    cell3.className = "text-center";
                    cell4.className = "text-center";
                }
            }
            else {
                alert_custom("W", "Nenhum produto da venda selecionada foi encontrado.");
            }
        }
    });

    sqlite.close();
}

function estorno_click(VEN_ID, PRO_ID, btn) {

    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let sql = `SELECT * FROM VENDAS_PRODUTOS WHERE VEN_ID = ${VEN_ID} AND PRO_ID = ${PRO_ID}`;

    sqlite.run(sql, [], function (err) {
        if (err.error != undefined) {
            alert_custom("E", "ERRO 1: " + err.error);
        }
        else {
            if (err.length > 0) {

                if ($("#rbAcampante").is(":checked")) {
                    sql = `INSERT INTO ESTORNO (VEN_ID, PRO_ID, EST_QTDE, EST_TOTAL, ACAMP_ID, USER_ID)
                      VALUES(${VEN_ID}, ${PRO_ID}, ${err[0].PRO_QTDE}, ${err[0].PRO_TOTAL}, ${$("#iptIdAcamp").val()}, ${sessionStorage.getItem("user_id")})`;
                }
                else {
                    sql = `INSERT INTO ESTORNO (VEN_ID, PRO_ID, EST_QTDE, EST_TOTAL, USER_ID)
                      VALUES(${VEN_ID}, ${PRO_ID}, ${err[0].PRO_QTDE}, ${err[0].PRO_TOTAL}, ${sessionStorage.getItem("user_id")})`;
                }
                let total = err[0].PRO_TOTAL;
                sqlite.run(sql, [], function (err) {
                    if (err.error != undefined) {
                        alert_custom("E", "ERRO 2: " + err.error);
                    }
                    else {
                        sql = `DELETE FROM VENDAS_PRODUTOS WHERE VEN_ID = ${VEN_ID} AND PRO_ID = ${PRO_ID}`;
                        sqlite.run(sql, [], function (err) {
                            if (err.error != undefined) {
                                alert_custom("E", "ERRO 3: " + err.error);
                            } else {
                                sql = `SELECT * FROM VENDAS_PRODUTOS WHERE VEN_ID = ${VEN_ID}`;

                                sqlite.run(sql, [], function (err) {
                                    if (err.error != undefined) {
                                        alert_custom("E", "ERRO 4: " + err.error);
                                    }
                                    else {
                                        if (err.length === 0) {
                                            sql = `DELETE FROM VENDAS WHERE VEN_ID = ${VEN_ID}`;
                                            sqlite.run(sql, [], function (err) {
                                                if (err.error != undefined) {
                                                    alert_custom("E", "ERRO 5: " + err.error);
                                                }
                                                else {
                                                    let tr_delete = $(btn).closest('tr');
                                                    let tr_prod = $(`#tbProds_${VEN_ID}`).closest('tr');
                                                    let tr_ven = $(`#tr_${VEN_ID}`);
                                                    tr_delete.remove();
                                                    tr_prod.remove();
                                                    tr_ven.remove();
                                                    alert_custom("S", "Venda estornada");
                                                }
                                            });
                                        }
                                        else {
                                            sql = `UPDATE VENDAS SET VEN_TOTAL = (VEN_TOTAL - ${total}) WHERE VEN_ID = ${VEN_ID}`;
                                            sqlite.run(sql, [], function (err) {
                                                if (err.error != undefined) {
                                                    alert_custom("E", "ERRO 5: " + err.error);
                                                }
                                                else {
                                                    let tr_delete = $(btn).closest('tr');
                                                    tr_delete.remove();
                                                    alert_custom("S", "Item estornado.");
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }
    });
}

function getAcampantes() {
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let sql = `SELECT * FROM ACAMPANTE WHERE upper(ACAMP_NAME) LIKE upper('%${$("#iptNameAcampM").val()}%');`;

    let row = sqlite.run(sql);

    if (row.length > 0) {
        $("#tbAcamp").find("tr:gt(0)").remove();
        let table = $("#tbAcamp")[0];

        for (let i = 0; i < row.length; i++) {
            let rowTable = table.insertRow(table.rows.length);

            let cell1 = rowTable.insertCell(0);
            let cell2 = rowTable.insertCell(1);
            let cell3 = rowTable.insertCell(2);
            let cell4 = rowTable.insertCell(3);

            cell1.innerHTML = `<button type='button' class='btn btn-sm btn-link' onclick='selectAcamp(${row[i].ACAMP_ID});'>${row[i].ACAMP_ID}</button>`;
            cell2.innerHTML = `<button type='button' class='btn btn-sm btn-link' onclick='selectAcamp(${row[i].ACAMP_ID});'>${row[i].ACAMP_NAME}</button>`;
            cell3.innerHTML = row[i].ACAMP_DT_NASC;
            cell4.innerHTML = row[i].ACAMP_FONE;

            cell1.className = "text-center";
            cell3.className = "text-center";
            cell4.className = "text-center";
        }
    }
    else {
        alert_custom("W", "Acampante não encontrado.");
    }
}

function selectAcamp(id) {
    $("#iptIdAcamp").val(id);
    $('#ModalPesqAcamp').modal('hide');
    getNameAcamp_Blur();
}