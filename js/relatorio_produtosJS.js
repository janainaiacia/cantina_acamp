$(document).ready(function (){

    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let sql = `SELECT P.PRO_DESCRIPTION, SUM(COALESCE(EP.ENT_QTDE, 0))  AS QTDE_ENT,
    SUM(COALESCE(V.PRO_QTDE, 0))  AS QTDE_SAIDA,
    SUM(COALESCE(V.PRO_TOTAL, 0)) AS VALOR_VENDIDO
    FROM PRODUTOS P
    LEFT JOIN ENTRADA_PRODUTOS EP ON  EP.PRO_ID = P.PRO_ID
    LEFT JOIN VENDAS_PRODUTOS V ON P.PRO_ID = V.PRO_ID
    GROUP BY P.PRO_DESCRIPTION
    ORDER BY PRO_DESCRIPTION`;

    sqlite.run(sql, [], function (err) {
        if (err.error != undefined) {
            alert_custom("E", err.error);
        } else {
            if (err.length > 0) {
                $("#tbRelatorio").find("tr:gt(0)").remove();
                let table = $("#tbRelatorio")[0];
                $("#div-relatorio").show();
                for (let i = 0; i < err.length; i++) {
                    let rowTable = table.insertRow(table.rows.length);

                    let cell1 = rowTable.insertCell(0);
                    let cell2 = rowTable.insertCell(1);
                    let cell3 = rowTable.insertCell(2);
                    let cell4 = rowTable.insertCell(3);

                    cell1.innerHTML = err[i].PRO_DESCRIPTION;
                    cell2.innerHTML = err[i].QTDE_ENT;
                    cell3.innerHTML = err[i].QTDE_SAIDA;
                    cell4.innerHTML = number_format(err[i].VALOR_VENDIDO, 2, ',', '.');

                    cell2.className = "text-center";
                    cell3.className = "text-center";
                    cell4.className = "text-center";
                    
                }
            }
            else {
                alert_custom("W", "Nenhum estorno encontrado");
                $("#div-relatorio").hide();
            }
        }
    });
});