$(document).ready(function () {

    $("#iptVlPago").maskMoney();

    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');
    var rows = sqlite.run("SELECT * FROM PRODUTOS WHERE PRO_ACTIVE = 'S'");

    var total = 0;
    var div_row = "<div class='row' style='height:40px;'>";
    var div_cell = "<div class='col-2'>";
    var div_button = "<div class='col-1'>";
    var div_close = "</div>";

    var label = `<label id='lblQtdeProd_[PRO_ID]' name='lblQtdeProd_[PRO_ID]' style='color:purple;'>-</label>`;
    var labelTotal = `<label id='lblTotalProd_[PRO_ID]' name='lblTotalProd_[PRO_ID]' style='color:purple;' class='total'>-</label>`;

    var button_add = `<button id='btnAdd[PRO_ID]' type='button' class='btn btn-sm btn-outline-success' onclick='addProduto([PRO_ID]);'>
        <i class='fa fa-plus'></i>
    </button>`;
    var button_del = `<button id='btnDel[PRO_ID]' type='button' class='btn btn-sm btn-outline-danger' onclick='delProduto([PRO_ID]);'>
        <i class='fa fa-minus'></i>
    </button>`;

    var div = $("#div-produtos");
    var append = '';
    for (var i = 0; i < rows.length; i++) {
        if (total === 0) {
            append = div_row;
        }

        append += div_cell + rows[i].PRO_DESCRIPTION.toString() + div_close +
            div_button + label.replaceAll('[PRO_ID]', rows[i].PRO_ID.toString()) + div_close +
            div_button + labelTotal.replaceAll('[PRO_ID]', rows[i].PRO_ID.toString()) + div_close +
            div_button + button_add.replaceAll('[PRO_ID]', rows[i].PRO_ID.toString()) + div_close +
            div_button + button_del.replaceAll('[PRO_ID]', rows[i].PRO_ID.toString()) + div_close;

        if(rows.length == 1){
            div.append(append);
        }
        if (total === 1) {
            total = 0;
            div.append(div_close + append);
        }
        else
            total++;
    }

    $("input[name='radioPagto']").on("change", function () {
        if ($("#rbCartao").is(":checked")) {
            $("#div-troco").hide();
            $("#iptVlPago").prop("disabled", true);
        }
        else {
            $("#div-troco").show();
            $("#iptVlPago").prop("disabled", false);
        }
    });
});


function getNameAcamp_Blur() {
    if ($("#iptIdAcamp").val() !== "") {

        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');
        let sql = `SELECT  A.ACAMP_NAME, 
        coalesce(F.FICHA_VALOR - coalesce(SUM(V.VEN_TOTAL), 0), 0) AS VALOR_REST,
        F.FICHA_ID  
        FROM ACAMPANTE A
        JOIN ACAMPANTE_FICHA F ON F.ACAMP_ID = A.ACAMP_ID AND F.FICHA_ATIVA = 'S'
        LEFT JOIN VENDAS V ON A.ACAMP_ID = V.ACAMP_ID AND V.FICHA_ID = F.FICHA_ID
         WHERE A.ACAMP_ID = ${$("#iptIdAcamp").val()}`;
        let row = sqlite.run(sql);

        if (row.length > 0 && row[0].ACAMP_NAME != null) {
            $("#iptNameAcamp").val(row[0].ACAMP_NAME);
            $("#iptFicha").val(number_format(row[0].VALOR_REST));
            $("#iptFichaFixa").val(row[0].VALOR_REST);
            $("#iptFichaID").val(row[0].FICHA_ID);
        }
        else {
            alert_custom("W", "Acampante não cadastrado ou sem ficha aberta.");
            $("#iptIdAcamp").val("");
            $("#iptNameAcamp").val("");
            $("#iptFicha").val("");
            $("#iptFichaFixa").val("");
            $("#iptFichaID").val("");
            $("#iptIdAcamp").focus();
        }
    }
    else {
        $("#iptIdAcamp").val("");
        $("#iptNameAcamp").val("");
        $("#iptFicha").val("");
        $("#iptFichaFixa").val("");
        $("#iptFichaID").val("");
    }
}

function getAcampantes() {
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let sql = `SELECT * FROM ACAMPANTE WHERE upper(ACAMP_NAME) LIKE upper('%${$("#iptNameAcampM").val()}%');`;

    let row = sqlite.run(sql);

    if (row.length > 0) {
        $("#tbAcamp").find("tr:gt(0)").remove();
        let table = $("#tbAcamp")[0];

        for (let i = 0; i < row.length; i++) {
            let rowTable = table.insertRow(table.rows.length);

            let cell1 = rowTable.insertCell(0);
            let cell2 = rowTable.insertCell(1);
            let cell3 = rowTable.insertCell(2);
            let cell4 = rowTable.insertCell(3);

            cell1.innerHTML = `<button type='button' class='btn btn-sm btn-link' onclick='selectAcamp(${row[i].ACAMP_ID});'>${row[i].ACAMP_ID}</button>`;
            cell2.innerHTML = `<button type='button' class='btn btn-sm btn-link' onclick='selectAcamp(${row[i].ACAMP_ID});'>${row[i].ACAMP_NAME}</button>`;
            cell3.innerHTML = row[i].ACAMP_DT_NASC;
            cell4.innerHTML = row[i].ACAMP_FONE;

            cell1.className = "text-center";
            cell3.className = "text-center";
            cell4.className = "text-center";
        }
    }
    else {
        alert_custom("W", "Acampante não encontrado.");
    }
}

function selectAcamp(id) {
    $("#iptIdAcamp").val(id);
    $('#ModalPesqAcamp').modal('hide');
    getNameAcamp_Blur();
}

function addProduto(PRO_ID) {
    let valida = true;
    if ($("#ckbVisitante").is(":checked")) {
        valida = false;
    }
    let validado = true;

    if (valida) {
        if ($("#iptIdAcamp").val() === '' || $("#iptNameAcamp").val() === '') {
            validado = false;
            alert_custom("W", "Informe o acampante.");
            $("#iptIdAcamp").focus();
        }
    }
    if (validado) {
        let qtde = $("#lblQtdeProd_" + PRO_ID)[0].textContent;

        if (qtde === "-")
            qtde = 1;
        else
            qtde = parseInt(qtde) + 1;

        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let sql = `SELECT max(PRO_QTDE) AS QTDE_MAX, printf("%.2f", PRO_PRECO) AS PRO_PRECO  
        FROM PRODUTOS_PRECO WHERE PRO_ID = ${PRO_ID}
        AND PRO_QTDE <= ${qtde} 
        GROUP BY PRO_PRECO ORDER BY max(PRO_QTDE) DESC LIMIT 1`;

        sqlite.run(sql, [], function (err) {
            if (err.error != undefined) {
                alert_custom("E", err.error);
            }
            else {
                let total = 0, result_div, qtdeT = qtde;
                if (err.length > 0) {
                    while (qtdeT > 0) {

                        result_div = parseInt(qtdeT / err[0].QTDE_MAX);

                        total = total + (parseFloat(err[0].PRO_PRECO) * result_div);

                        qtdeT = qtdeT - (result_div * err[0].QTDE_MAX)
                    }
                    let ficha = parseFloat($("#iptFichaFixa").val());
                    let totalProd = 0;

                    $('.total').each(
                        function () {
                            if ($(this)[0].textContent !== '-' && this.id.indexOf(PRO_ID) === -1)
                                totalProd = totalProd + parseFloat($(this)[0].textContent.replaceAll(".", "").replaceAll(",", ".").replace("", "0"));
                        });

                    totalProd = totalProd + total;
                    let total_restante = ficha - totalProd;
                    if (total_restante < 0) {
                        alert_custom("W", "Acampante não possui saldo suficiente.");
                        return false;
                    }
                    else {
                        $("#iptFicha").val(number_format(total_restante));
                        $("#lblQtdeProd_" + PRO_ID)[0].textContent = qtde;
                        $("#lblTotalProd_" + PRO_ID)[0].textContent = number_format(parseFloat(total).toFixed(2));
                        calculaTotal();
                    }

                } else {
                    alert_custom("E", "Erro do pesquisar preço.");
                }
            }
        });
        sqlite.close();
    }
}

function delProduto(PRO_ID) {
    let qtde = $("#lblQtdeProd_" + PRO_ID)[0].textContent;

    if (qtde == "-")
        return false;
    else {
        qtde = parseInt(qtde) - 1;

        if (qtde == 0) {
            $("#lblQtdeProd_" + PRO_ID)[0].textContent = "-";
            $("#lblTotalProd_" + PRO_ID)[0].textContent = "-";
            calculaTotal();
        }
        else {
            $("#lblQtdeProd_" + PRO_ID)[0].textContent = qtde;

            var sqlite = require('sqlite-sync');
            sqlite.connect(__dirname + '/database.sqlite');

            let sql = `SELECT max(PRO_QTDE) AS QTDE_MAX, printf("%.2f", PRO_PRECO) AS PRO_PRECO  
            FROM PRODUTOS_PRECO WHERE PRO_ID = ${PRO_ID}
            AND PRO_QTDE <= ${qtde} 
            GROUP BY PRO_PRECO ORDER BY max(PRO_QTDE) DESC LIMIT 1`;

            sqlite.run(sql, [], function (err) {
                if (err.error != undefined) {
                    alert_custom("E", err.error);
                }
                else {
                    let total = 0, result_div;
                    if (err.length > 0) {
                        while (qtde > 0) {

                            result_div = parseInt(qtde / err[0].QTDE_MAX);

                            total = total + (parseFloat(err[0].PRO_PRECO) * result_div);

                            qtde = qtde - (result_div * err[0].QTDE_MAX)
                        }
                        $("#lblTotalProd_" + PRO_ID)[0].textContent = number_format(parseFloat(total).toFixed(2));
                        calculaTotal();

                    } else {
                        alert_custom("E", "Erro do pesquisar preço.");
                    }
                }
            });
            sqlite.close();
        }
    }
}

function calculaTotal() {
    var total = 0;
    $('.total').each(
        function () {
            if ($(this)[0].textContent !== '-')
                total = total + parseFloat($(this)[0].textContent.replace(',', '.').replace("", "0"));
        });

    let ficha = parseFloat($("#iptFichaFixa").val());
    let total_restante = ficha - total;
    $("#iptFicha").val(number_format(total_restante, 2));
    $("#iptTotal").val(number_format(total, 2));

    if ($("#rbCartao").is(":checked")) {
        $("#iptVlPago").val(number_format(total, 2));
    }
}

function check_visitante() {
    $("#iptVlPago").val("");
    $("#iptTroco").val("");

    if ($("#ckbVisitante").is(":checked")) {
        $("#iptIdAcamp").prop("disabled", true);
        $("#btnPesqAcamp").prop("disabled", true);
        $("#div-tp-pagto").show();
        $("#div-valor-ficha").hide();
        $("#div-valor-pago").show();
        $("#div-troco").show();
    } else {
        $("#iptIdAcamp").prop("disabled", false);
        $("#btnPesqAcamp").prop("disabled", false);
        $("#div-tp-pagto").hide();
        $("#div-valor-ficha").show();
        $("#div-valor-pago").hide();
        $("#div-troco").hide();
    }
}

function btnNovo_Click() {
    $("#ckbVisitante").prop("checked", false);
    check_visitante();
    $("#iptIdAcamp").val("");
    $("#iptNameAcamp").val("");
    $("input[name='radioPagto']").prop("checked", false);
    $("#iptFichaFixa").val("");
    $("#iptFicha").val("");
    $("#iptTotal").val("0,00");
    $("#iptVlPago").val("");
    $("#iptTroco").val("");
    $("#iptIdAcamp").focus();

    $("label[name^='lblTotalProd_']").each(function () {
        $(this)[0].textContent = "-";
    });

    $("label[name^='lblQtdeProd_']").each(function () {
        $(this)[0].textContent = "-";
    });
}

function calculaTroco() {
    let total = parseFloat($("#iptTotal").val().replaceAll(".", "").replaceAll(",", "."));
    let vl_pago = parseFloat($("#iptVlPago").val().replaceAll(".", "").replaceAll(",", "."));

    $("#iptTroco").val(number_format(vl_pago - total));
}

function btnGravar_Click() {
    if (validaCampos()) {
        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');
        let rollback = false;
        let vendaID = 0;
        try {
            let sql = `INSERT INTO VENDAS 
            (VEN_TOTAL, USER_ID, FICHA_ID, ACAMP_ID) VALUES
            (${$("#iptTotal").val().replaceAll(".", "").replaceAll(",", ".")}, 
                ${sessionStorage.getItem("user_id")},
                ${$("#iptFichaID").val()},
                ${$("#iptIdAcamp").val()})`;

            if ($("#ckbVisitante").is(":checked")) {
                let tp_pagto = $("input[name='radioPagto']:checked").val();

                sql = `INSERT INTO VENDAS 
                (VEN_TOTAL, USER_ID, VEN_VISITANTE, VEN_TP_PAGTO) VALUES
                (${$("#iptTotal").val().replaceAll(".", "").replaceAll(",", ".")}, 
                ${sessionStorage.getItem("user_id")},
                'S',
                '${tp_pagto}')`;
            }

            sqlite.run(sql, [], function (err) {
                if (err.error != undefined) {
                    rollback = true;
                }
                else {
                    vendaID = err;
                    sql = `INSERT INTO VENDAS_PRODUTOS
                        (VEN_ID, PRO_ID, PRO_QTDE, PRO_TOTAL)
                        VALUES (?,?,?,?)`;
                    let param = [];
                    let ven_id = err;
                    let pro_id, pro_qtde, pro_total;

                    $(".total").each(function () {
                        if ($(this)[0].textContent !== '-') {
                            pro_total = $(this)[0].textContent.replaceAll(".", "").replaceAll(",", ".");
                            let lblName = this.id.replaceAll("lblTotalProd_", "lblQtdeProd_");
                            pro_qtde = $("#" + lblName)[0].textContent;
                            pro_id = this.id.replaceAll("lblTotalProd_", "");
                            param = [ven_id, pro_id, pro_qtde, pro_total];
                            sqlite.run(sql, param, function (err) {
                                if (err.error != undefined) {
                                    rollback = true;
                                };
                            });

                        }
                    });
                }
            });
        }
        catch (e) {
            rollback = true;
        }
        finally {
            if (rollback) {
                sqlite.run(`DELETE FROM VENDAS_PRODUTOS WHERE VEN_ID = ${vendaID}`);
                sqlite.run(`DELETE FROM VENDAS WHERE VEN_ID = ${vendaID}`);
                alert_custom("W", "Não foi possível gravar a venda.");
            } else {
                alert_custom("S", "Venda salva!");
                btnNovo_Click();
            }
        }
    }
}

function validaCampos() {
    let msg = "";
    let total = parseFloat($("#iptTotal").val().replaceAll(".", "").replaceAll(",", "."));

    if ($("#ckbVisitante").is(":checked")) { // visitante
        if ($("input[name='radioPagto']").is(":checked")) {
            let vl_pagto = 0;
            try {
                if ($("#iptVlPago").val() !== "")
                    vl_pagto = parseFloat($("#iptVlPago").val().replace(".", "").replaceAll(",", "."));//converter
            }
            catch{
                vl_pagto = 0;
            }

            if ($("#rbDinheiro").is(":checked") && vl_pagto === 0) {
                msg += "Informe o valor recebido." + "<br />";
            }
            else {
                if (total > vl_pagto) {
                    msg += "O valor total não foi pago. Verifique!" + "<br />";
                }
            }
        }
        else {
            msg += "Informe o tipo de pagamento." + "<br />";
        }
    }
    else {
        //Acampante
        if ($("#iptIdAcamp").val() === '' || $("#iptNameAcamp").val() === '') {
            msg += "Informe o acampante." + "<br />";
        }
        else {
            let ficha = 0;
            if ($("#iptFichaFixa").val() !== "")
                ficha = parseFloat($("#iptFichaFixa").val());

            if (ficha - total < 0)
                msg += "Acampante sem saldo suficiente." + "<br />";
        }
    }

    if (msg !== "") {
        alert_custom("W", msg);
        return false;
    }
    else
        return true;
}

function setValorPago() {

    if ($("input[name='radioPagto']:checked").val() === 'C') {
        $("#iptVlPago").val($("#iptTotal").val());
    }
    else {
        $("#iptVlPago").val("");
        $("#iptTroco").val("");
    }
}

/******************RELATÓRIO******************/

function FormataStringData(data) {
    var dia = data.split("/")[0];
    var mes = data.split("/")[1];
    var ano = data.split("/")[2];

    return ano + '-' + ("0" + mes).slice(-2) + '-' + ("0" + dia).slice(-2);
    // Utilizo o .slice(-2) para garantir o formato com 2 digitos.
}

function btnBuscar_Click() {
    if ($("#iptDtIni").val() !== "" && $("#iptDtIni").val() !== "") {
        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let sql = `SELECT V.VEN_ID, strftime('%d/%m/%Y',V.VEN_DT_REG) AS VEN_DT_REG,
        CASE WHEN V.ACAMP_ID IS NULL THEN 'VISITANTE' ELSE A.ACAMP_NAME
        END AS ACAMP_NAME, CASE WHEN V.VEN_TP_PAGTO = 'D' THEN
        'DINHEIRO' WHEN V.VEN_TP_PAGTO = 'C' THEN  'CARTÃO'
        ELSE 'FICHA'END AS VEN_TP_PAGTO, V.VEN_TOTAL, U.USER_USERNAME
        FROM VENDAS V INNER JOIN USER U ON U.USER_ID = V.USER_ID
        LEFT JOIN ACAMPANTE A ON A.ACAMP_ID = V.ACAMP_ID
        WHERE V.VEN_DT_REG BETWEEN 
        date('${FormataStringData($("#iptDtIni").val())}') 
        AND date(${FormataStringData($("#iptDtIni").val())})`

        sqlite.run(sql, [], function (err) {
            if (err.error != undefined) {
                alert_custom("E", err.error);
            } else {
                if (err.length > 0) {
                    $("#tbRelatorio").find("tr:gt(0)").remove();
                    let table = $("#tbRelatorio")[0];
                    $("#div-relatorio").show();
                    for (let i = 0; i < row.length; i++) {
                        let rowTable = table.insertRow(table.rows.length);

                        let cell1 = rowTable.insertCell(0);
                        let cell2 = rowTable.insertCell(1);
                        let cell3 = rowTable.insertCell(2);
                        let cell4 = rowTable.insertCell(3);
                        let cell5 = rowTable.insertCell(4);
                        let cell6 = rowTable.insertCell(5);

                        cell1.innerHTML = `<button type='button' class='btn btn-sm btn-outline-secondary' onclick='selectPlus(${err[i].VEN_ID}, this);'>
                                                <i class="fas fa-angle-down"></i>
                                            </button>`;
                        cell2.innerHTML = err[i].VEN_DT_REG;
                        cell3.innerHTML = err[i].ACAMP_NAME;
                        cell4.innerHTML = err[i].VEN_TP_PAGTO;
                        cell5.innerHTML = number_format(err[i].VEN_TOTAL, 2);
                        cell6.innerHTML = err[i].USER_USERNAME;
                    }
                }
                else {
                    alert_custom("W", "Nenhum venda encontrada");
                    $("#div-relatorio").hide();
                }
            }
        })
    }
    else {
        alert_custom("W", "Preencha as datas para a pesquisa. ")
    }
}