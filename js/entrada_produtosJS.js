function getNameProd_Blur() {
    if ($("#iptIdProd").val() !== "") {

        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let sql = `SELECT * FROM PRODUTOS WHERE PRO_ID = ?`;

        sqlite.run(sql, [$("#iptIdProd").val()], function (err) {
            if (err.error != undefined) {
                alert_custom("E", err.error);
            }
            else {
                if (err.length > 0) {
                    $("#iptNameProd").val(err[0].PRO_DESCRIPTION);
                    $("#iptQtde").focus();
                }
                else {
                    alert_custom("W", "Produto não encontrado.")
                    $("#iptNameProd").val("");
                    $("#iptQtde").val("");
                }
            }
        });
    }
    else {
        $("#iptNameProd").val("");
        $("#iptQtde").val("");
    }

}

function btnEntrada_Click() {
    if (validaCampos()) {
        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let sql = `INSERT INTO ENTRADA_PRODUTOS(PRO_ID, ENT_QTDE, ENT_DT_REG, USER_ID) VALUES
        (${$("#iptIdProd").val()}, ${$("#iptQtde").val()}, CURRENT_TIMESTAMP, ${sessionStorage.getItem("user_id")})`;

        sqlite.run(sql, [], function (err) {
            if (err.error != undefined) {
                alert_custom("E", err.error);
            }
            else {
                $("#iptIdProd").val("");
                $("#iptNameProd").val("");
                $("#iptQtde").val("");
                $("#iptIdProd").focus();
                alert_custom("S", "Entrada do produto salva com sucesso.");
            }
        });
    }
}

function validaCampos() {
    let msg = "";
    if ($("#iptIdProd").val() === "" || $("#iptNameProd").val() === "") {
        msg += "Informe o produto." + "<br />";
    }

    if ($("#iptQtde").val() === "" || $("#iptQtde").val() === "0") {
        msg += "Informe a quantidade de entrada." + "<br />";
    }

    if (msg === "")
        return true;
    else {
        alert_custom("W", msg);
        return false;
    }
}

function getProdutos() {
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    $("#tbProd").find("tr:gt(0)").remove();
    let table = $("#tbProd")[0];

    let sql = `SELECT * FROM PRODUTOS WHERE 
               upper(PRO_DESCRIPTION) LIKE 
               upper('%${$("#iptNameProdM").val()}%');`;

    sqlite.run(sql, [], function (err) {
        if (err.error != undefined) {
            alert_custom("E", err.error);
        }
        else {
            if (err.length > 0) {
                for (let i = 0; i < err.length; i++) {
                    let rowTable = table.insertRow(table.rows.length);

                    let cell1 = rowTable.insertCell(0);
                    let cell2 = rowTable.insertCell(1);

                    cell1.innerHTML = `<button type='button' class='btn btn-sm btn-link' onclick='selectProd(${err[i].PRO_ID});'>${err[i].PRO_ID}</button>`;
                    cell2.innerHTML = `<button type='button' class='btn btn-sm btn-link' onclick='selectProd(${err[i].PRO_ID});'>${err[i].PRO_DESCRIPTION}</button>`;

                    cell1.className = "text-center";
                }
            }
            else {
                alert_custom("W", "Produto não encontrado.");
            }
        }
    });
}

function selectProd(id) {
    $("#iptIdProd").val(id);
    $('#ModalPesqProd').modal('hide');
    getNameProd_Blur();
}

$(document).ready(function () {
    $("#iptDtIni").datepicker();
    $("#iptDtFim").datepicker();

    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = (day < 10 ? '0' : '') + day + '/' +
        (month < 10 ? '0' : '') + month + '/' +
        d.getFullYear();

    $("#iptDtIni").val(output);
    $("#iptDtFim").val(output);
});

function btnBuscar_Click() {
    if ($("#iptDtIni").val() !== "" && $("#iptDtFim").val() !== "") {

        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let sql = `SELECT strftime('%d/%m/%Y', E.ENT_DT_REG) AS ENT_DT_REG,
        P.PRO_DESCRIPTION, E.ENT_QTDE, U.USER_USERNAME, E.ENT_COD
        FROM ENTRADA_PRODUTOS E, PRODUTOS P, USER U
        WHERE E.PRO_ID = P.PRO_ID
        AND E.USER_ID = U.USER_ID
        AND E.ENT_DT_REG BETWEEN 
        date('${FormataStringData($("#iptDtIni").val())}') 
        AND date('${FormataStringData($("#iptDtFim").val())}')
        ORDER BY P.PRO_DESCRIPTION, date(E.ENT_DT_REG)`;

        sqlite.run(sql, [], function (err) {
            if (err.error != undefined) {
                alert_custom("E", err.error);
            }
            else {
                $("#tbEntrada").find("tr:gt(0)").remove();
                let table = $("#tbEntrada")[0];
                
                if (err.length > 0) {
                    for (let i = 0; i < err.length; i++) {
                        let rowTable = table.insertRow(table.rows.length);

                        let cell1 = rowTable.insertCell(0);
                        let cell2 = rowTable.insertCell(1);
                        let cell3 = rowTable.insertCell(2);
                        let cell4 = rowTable.insertCell(3);
                        let cell5 = rowTable.insertCell(4);

                        cell1.innerHTML = err[i].ENT_DT_REG;
                        cell2.innerHTML = err[i].PRO_DESCRIPTION;
                        cell3.innerHTML = err[i].ENT_QTDE;
                        cell4.innerHTML = err[i].USER_USERNAME;
                        cell5.innerHTML = `<button type='button' class='btn btn-sm'
                                style='color:red;' onclick='deleteEntrada(${err[i].ENT_COD}, this);'>
                                    <i class='fa fa-times'></i>
                                </button>`;

                        cell1.className = "text-center";
                        cell3.className = "text-center";
                        cell5.className = "text-center";
                    }
                }
                else {
                    alert_custom("W", "Nenhuma entrada encontrada.");
                    $("#div-relatorio").hide();
                }
            }
        });
    }
    else {
        alert_custom("w", "Preencha as datas corretamente.");
    }
}


function deleteEntrada(id, btn) {

    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let sql = `DELETE FROM ENTRADA_PRODUTOS WHERE ENT_COD = ${id}`;

    sqlite.run(sql, [], function (err) {
        if (err.error != undefined) {
            alert_custom("E", err.error);
        }
        else {
            let tr_delete = $(btn).closest('tr');
            tr_delete.remove();

            alert_custom("S", "Entrada excluída.");
        }
    });
}