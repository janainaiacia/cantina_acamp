$(document).ready(function () {
    if(sessionStorage.getItem("admin") === "N"){
        $(".admin").each(function (){
            $(this).hide();
        });
    }

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

    $('[data-toggle="tooltip"]').tooltip();
    display_ct();

    if(sessionStorage.getItem("username") === null || sessionStorage.getItem("username")=== ''){
        alert("Usuário não está logado.");
        window.location = 'login.html';
    }

    $("#btnLogout").on("click", function(){
        logout();
    });
});

function display_c() {
    var refresh = 1000; // Refresh rate in milli seconds
    mytime = setTimeout('display_ct()', refresh)
}

function display_ct() {
    var x = new Date()

    // date part ///
    var month = x.getMonth() + 1;
    var day = x.getDate();
    var year = x.getFullYear();
    if (month < 10) { month = '0' + month; }
    if (day < 10) { day = '0' + day; }
    var x3 = day + '/' + month + '/' + year;

    // time part //
    var hour = x.getHours();
    var minute = x.getMinutes();
    var second = x.getSeconds();
    if (hour < 10) { hour = '0' + hour; }
    if (minute < 10) { minute = '0' + minute; }
    if (second < 10) { second = '0' + second; }
    var x3 = x3 + ' ' + hour + ':' + minute + ':' + second;

    $("#lblData")[0].textContent = x3;
    $("#lblUser")[0].textContent = sessionStorage.getItem("username");

    display_c();
}

function logout(){
    sessionStorage.setItem("user_id", "");
    sessionStorage.setItem("username", "");

    window.location="login.html";
}

function changeNav(anchor, form) {
    $("a[class*='active']").removeClass("active");
    $("#" + anchor).addClass("active");

    $("form").hide();
    $("#" + form).show();
}
