$(document).ready(function (){
    $("#iptdtNasc").datepicker();
})

function btnNovo_Click(){
    $("#iptIdAcamp").val('');
    $("#iptNameAcamp").val('');
    $("#iptdtNasc").val('');
    $("#iptTelefone").val('');

    $("#btnExcluir").hide();
    $("#iptIdAcamp").focus();
}

function btnSalvar_Click(){
    if(validaCampos()){
        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let sql = '';

        if($("#btnExcluir").is(":visible")){ //Update
            sql=`UPDATE ACAMPANTE SET ACAMP_NAME = '${$("#iptNameAcamp").val()}', 
                                      ACAMP_DT_NASC = '${$("#iptdtNasc").val()}', 
                                      ACAMP_FONE = '${$("#iptTelefone").val()}' 
                                      WHERE ACAMP_ID = ${$("#iptIdAcamp").val()}`;
        }        
        else{
            sql =`INSERT INTO ACAMPANTE (ACAMP_ID, ACAMP_NAME, ACAMP_DT_NASC, ACAMP_FONE) 
                   VALUES (${$("#iptIdAcamp").val()}, '${$("#iptNameAcamp").val()}', 
                   '${$("#iptdtNasc").val()}','${$("#iptTelefone").val()}');`;
        }

        sqlite.run(sql, [], function(err)
        {
            if(err.error != undefined){
                alert_custom("E", err.error);
            }
            else{
                alert_custom("S", "Acampante salvo com sucesso.");
                $("#btnExcluir").show();
            }
        });
    }
}

function btnExcluir_Click(){

    if($("#iptIdAcamp").val() === ''){
        alert_custom("W", "Informe o código do acampante.");
    }
    else{
        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let sql = `DELETE FROM ACAMPANTE WHERE ACAMP_ID = ${$("#iptIdAcamp").val()}`;
        let row = sqlite.run(sql);

        if(row === 0 || row === 1){//Sucesso
            btnNovo_Click();
            alert_custom("S", "Acampante excluído com sucesso.");
        }
        else{
            alert_custom("E", row.error);
        }
    }
}

function validaCampos(){
    var msg = '';
    if($("#iptIdAcamp").val() === ''){
        msg+= 'Informe o código do acampante.' + '<br />';
    }

    if($("#iptNameAcamp").val() === ''){
        msg+= 'Informe o nome do acampante.' + '<br />';
    }

    /*if($("#iptdtNasc").val() === ''){
        msg+= 'Informe a data de nascimento do acampante.' + '<br />';
    }*/

    if(msg != ''){
        alert_custom("W", msg);
        return false;
    }
    else{
        return true;
    }
}

function getAcampCod(cadastro){

    cadastro = cadastro || false;

    if($("#iptIdAcamp").val() !== ""){
        
        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');
        let sql = `SELECT * FROM ACAMPANTE WHERE ACAMP_ID = ${$("#iptIdAcamp").val()}`;
        let row = sqlite.run(sql);

        if(row.length > 0){
            $("#iptNameAcamp").val(row[0].ACAMP_NAME);
            $("#iptdtNasc").val(row[0].ACAMP_DT_NASC);
            $("#iptTelefone").val(row[0].ACAMP_FONE);

            $("#btnExcluir").show();
        }
        else
        {
            if(!cadastro)
            {
                alert_custom("W", "Acamapante não encontrado.");
            }
        }
    }
    else{
        btnNovo_Click();
    }
}

function getAcampLikeName(){

    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');
    let sql = `SELECT * FROM ACAMPANTE WHERE upper(ACAMP_NAME) LIKE upper('%${$("#iptNameAcampC").val()}%')
    ORDER BY ACAMP_ID;`;

    let row = sqlite.run(sql);

    if(row.length > 0){
        let table = $("#tbAcamp")[0];
        $("#tbAcamp").find("tr:gt(0)").remove();
        for(let i = 0; i < row.length; i++){
            let rowTable = table.insertRow(table.rows.length);

            let cell1 = rowTable.insertCell(0);
            let cell2 = rowTable.insertCell(1);
            let cell3 = rowTable.insertCell(2);
            let cell4 = rowTable.insertCell(3);
            let cell5 = rowTable.insertCell(4);
            let cell6 = rowTable.insertCell(5);

            cell1.innerHTML = row[i].ACAMP_ID;
            cell2.innerHTML = row[i].ACAMP_NAME;
            cell3.innerHTML = row[i].ACAMP_DT_NASC;
            cell4.innerHTML = row[i].ACAMP_FONE;

            cell5.innerHTML = `<button type='button' class='btn btn-sm'
                                    onclick='editAcamp(${row[i].ACAMP_ID});'>
                                    <i class="fas fa-edit"></i>
                            </button>`;
            
            cell6.innerHTML = `<button type='button' class='btn btn-sm'
                style='color:red;' onclick='deleteAcamp(${row[i].ACAMP_ID}, this);'>
                <i class='fa fa-times'></i>
            </button>`;
        }
    }
}

function editAcamp(id){
    $("#iptIdAcamp").val(id);
    getAcampCod(true);
    changeNav('a1', 'form-cadastro');
}

function deleteAcamp(id, btn){
    var sqlite = require('sqlite-sync');
    sqlite.connect(__dirname + '/database.sqlite');

    let sql = `DELETE FROM ACAMPANTE WHERE ACAMP_ID = ${id}`;
    let row = sqlite.run(sql);

    if(row === 0 || row === 1){//Sucesso
        let tr_delete = $(btn).closest('tr');
        tr_delete.remove();

        alert_custom("S", "Acampante excluído com sucesso.");
        btnNovo_Click();
    }
    else{
        alert_custom("E", row.error);
    }
}