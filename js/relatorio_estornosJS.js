function btnBuscar_Click() {
    if ($("#iptDtIni").val() !== "" && $("#iptDtFim").val() !== "") {

        var sqlite = require('sqlite-sync');
        sqlite.connect(__dirname + '/database.sqlite');

        let sql = `SELECT CASE WHEN E.ACAMP_ID IS NULL THEN 'VISITANTE'
                ELSE A.ACAMP_NAME END AS ACAMP_NAME, P.PRO_DESCRIPTION, E.EST_QTDE,
                E.EST_TOTAL, strftime('%d/%m/%Y', E.EST_DT_REG) AS EST_DT_REG,
                U.USER_USERNAME FROM ESTORNO E
                INNER JOIN USER U ON E.USER_ID = U.USER_ID
                INNER JOIN PRODUTOS P ON P.PRO_ID = E.PRO_ID
                LEFT JOIN ACAMPANTE A ON E.ACAMP_ID = A.ACAMP_ID
                        WHERE date(E.EST_DT_REG) BETWEEN 
            date('${FormataStringData($("#iptDtIni").val())}') 
            AND date('${FormataStringData($("#iptDtFim").val())}');`

        sqlite.run(sql, [], function (err) {
            if (err.error != undefined) {
                alert_custom("E", err.error);
            } else {
                if (err.length > 0) {
                    $("#tbRelatorio").find("tr:gt(0)").remove();
                    let table = $("#tbRelatorio")[0];
                    $("#div-relatorio").show();
                    for (let i = 0; i < err.length; i++) {
                        let rowTable = table.insertRow(table.rows.length);

                        let cell1 = rowTable.insertCell(0);
                        let cell2 = rowTable.insertCell(1);
                        let cell3 = rowTable.insertCell(2);
                        let cell4 = rowTable.insertCell(3);
                        let cell5 = rowTable.insertCell(4);
                        let cell6 = rowTable.insertCell(5);

                        cell1.innerHTML = err[i].ACAMP_NAME;
                        cell2.innerHTML = err[i].PRO_DESCRIPTION;
                        cell3.innerHTML = err[i].EST_QTDE;
                        cell4.innerHTML = number_format(err[i].EST_TOTAL, 2, ',', '.');
                        cell5.innerHTML = err[i].EST_DT_REG;
                        cell6.innerHTML = err[i].USER_USERNAME;

                        cell3.className = "text-center";
                        cell4.className = "text-center";
                        cell5.className = "text-center";
                    }
                }
                else {
                    alert_custom("W", "Nenhum estorno encontrado");
                    $("#div-relatorio").hide();
                }
            }
        });
    }
    else{
        alert_custom("w", "Preencha as datas corretamente.")
    }
}

/******************RELATÓRIO******************/
$(document).ready(function () {
    $("#iptDtIni").datepicker();
    $("#iptDtFim").datepicker();

    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = (day < 10 ? '0' : '') + day + '/' +
        (month < 10 ? '0' : '') + month + '/' +
        d.getFullYear();

    $("#iptDtIni").val(output);
    $("#iptDtFim").val(output);
});