// ./build_installer.js

// 1. Import Modules
const { MSICreator } = require('electron-wix-msi');
const path = require('path');

// 2. Define input and output directory.
const APP_DIR = path.resolve(__dirname, './release-builds/cantina-app-win32-x64');
const OUT_DIR = path.resolve(__dirname, './cantina_installer');

// 3. Instantiate the MSICreator
const msiCreator = new MSICreator({
    appDirectory: APP_DIR,
    outputDirectory: OUT_DIR,

    // Configure metadata
    description: 'Controle Cantina Acampamento',
    exe: 'cantina-app',
    appIconPath: path.resolve(__dirname, './icon.ico'),
    name: 'Cantina Acampamento',
    manufacturer: "Cantina Acampamento Inc",
    version: '1.0.0',

    // Configure installer User Interface
    ui: {
        chooseDirectory: true
    },
});

// 4. Create a .wxs template file
msiCreator.create().then(function () {

    // Step 5: Compile the template to a .msi file
    msiCreator.compile();
});